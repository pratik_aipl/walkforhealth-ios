import UIKit

class SupportUsVC: UIViewController {
    //MARK: - Properties
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var viewLogo: UIView!
    @IBOutlet weak var labelSupports: UILabel!
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        self.viewNavigation.backgroundColor = Define.APP_COLOR
        self.viewLogo.backgroundColor = Define.APP_COLOR
        self.labelSupports.textColor = Define.APP_COLOR
    }
    
    //MARK: - Button Method
    @IBAction func buttonBack(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
}
