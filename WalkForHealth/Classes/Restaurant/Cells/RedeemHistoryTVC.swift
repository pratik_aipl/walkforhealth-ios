import UIKit

class RedeemHistoryTVC: UITableViewCell {

    @IBOutlet weak var labelWalkerName: UILabel!
    @IBOutlet weak var labelWalkingDistance: UILabel!
    @IBOutlet weak var labalWalkingDate: UILabel!
    @IBOutlet weak var labelDiscount: UILabel!
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var labelOfferName: UILabel!
    @IBOutlet weak var labelPromoCode: UILabel!
    @IBOutlet weak var buttonRedeem: UIButton!
    @IBOutlet weak var constraintRedeemViewHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Design().viewWithShadow(view: viewCell)
        Design().setLabel(label: labelWalkerName)
        Design().setLabel(label: labelDiscount)
        Design().setLabel(label: labelOfferName)
        Design().setButton(button: buttonRedeem)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }

}
