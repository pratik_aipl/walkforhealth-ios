import UIKit

class UserTVC: UITableViewCell {

  
    @IBOutlet weak var lbldiscount: UILabel!
    @IBOutlet weak var lbloffername: UILabel!
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelLimit: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Design().viewWithShadow(view: viewCell)
        Design().setLabel(label: labelUserName)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }

}
