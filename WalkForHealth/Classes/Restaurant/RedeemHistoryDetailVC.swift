import UIKit
import GoogleMaps

class RedeemHistoryDetailVC: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var viewRedeemDetail: UIView!
    @IBOutlet weak var labelWalkerName: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelDiscount: UILabel!
    @IBOutlet weak var labelOfferName: UILabel!
    @IBOutlet weak var labelPromoCode: UILabel!
    
    @IBOutlet weak var viewMap: GMSMapView!
    
    var dictRedeemDetail = [String: Any]()
    
    
    private var arrOfferDetail = [[String: Any]]()
    private var path = GMSMutablePath()
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()
        if !MyModel().isConnectedToInternet() {
            MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET,
                                      alertController:  self)
        } else {
            getOfferDataAPI()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        self.viewNavigation.backgroundColor = Define.APP_COLOR
        
        viewRedeemDetail.layer.cornerRadius = 8
        
        labelWalkerName.text = dictRedeemDetail["Name"] as? String
        
        if let distance = dictRedeemDetail["MinimumKM"] as? String {
            labelDistance.text = "\(distance) KM"
        } else {
            labelDistance.text = "0 KM"
        }
        
        if let discount = dictRedeemDetail["Discount"] as? String {
            labelDiscount.text = "\(discount)%"
        } else {
            labelDiscount.text = "0%"
        }
        
        labelDate.text = "\(MyModel().dateFormatterStringToString2(date: dictRedeemDetail["StartDate"] as! String)) TO \(MyModel().dateFormatterStringToString2(date: dictRedeemDetail["EndDate"] as! String))"
        
        labelOfferName.text = dictRedeemDetail["OfferTitle"] as? String
        labelPromoCode.text = dictRedeemDetail["CouponCode"] as? String
    }
    
    //MARK: - Button Method
    @IBAction func buttonBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - API
extension RedeemHistoryDetailVC {
    func getOfferDataAPI() {
        
        Define.APPDELEGATE.showLoadingView()

        let parameter: [String: Any] = ["WalkerOfferID": dictRedeemDetail["WalkerOfferID"]!]
        let strURL = Define.API_BASE_URL + Define.API_USER_OFFER_DETAIL
        print("Parameter: ", parameter, "\nURL: ", strURL)
        SwiftAPI().postGetActiveOfferMethod(stringURL: strURL,
                                            userID: Define.USERDEFAULT.value(forKey: "UserID") as! String,
                                            parameter: parameter,
                                            header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            if let error = error {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.getOfferDataAPI()
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    self.arrOfferDetail = result!["data"] as! [[String: Any]]
                    self.setPath()
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
    
    func setPath() {
        if self.arrOfferDetail.count > 0 {
            let dictOfferDetail = arrOfferDetail[0]
            if let arrData = dictOfferDetail["MapData"] as? [[String: Any]] {
                let arrPath = arrData
                if arrPath.count > 1 {
                    
                    for (index, dictData) in arrPath.enumerated() {
                        let lat = Double(dictData["Latitude"] as! String)
                        let long = Double(dictData["Longitude"] as! String)
                        path.addLatitude(lat!, longitude: long!)
                        let rectangle = GMSPolyline(path: path)
                        rectangle.strokeWidth = 2
                        rectangle.map = viewMap
                        
                        if index == 0 {
                            self.setMap(latitude: lat!, longitude: long!)
                            let position = CLLocationCoordinate2DMake(lat!, long!)
                            let marker = GMSMarker(position: position)
                            marker.icon = #imageLiteral(resourceName: "ic_streetviewicon")
                            marker.map = viewMap
                        } else if index == arrPath.count - 1 {
                            let position = CLLocationCoordinate2DMake(lat!, long!)
                            let marker = GMSMarker(position: position)
                            marker.map = viewMap
                        }
                    }
                } else if arrPath.count == 1 {
                    let dictData = arrPath[0]
                    let lat = Double(dictData["Latitude"] as! String)
                    let long = Double(dictData["Longitude"] as! String)
                    self.setMap(latitude: lat!, longitude: long!)
                    let position = CLLocationCoordinate2DMake(lat!, long!)
                    let marker = GMSMarker(position: position)
                    marker.icon = #imageLiteral(resourceName: "ic_streetviewicon")
                    marker.map = viewMap
                } else {
                    print("There is no path.")
                    
                }
            } else {
                
            }
        }
    }
    
    func setMap(latitude: Double, longitude: Double) {
        let camera = GMSCameraPosition.camera(withLatitude: latitude,
                                              longitude: longitude,
                                              zoom: 12)
        viewMap.animate(to: camera)
    }
}
