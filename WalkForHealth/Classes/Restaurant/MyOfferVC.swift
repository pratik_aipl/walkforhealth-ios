import UIKit

class MyOfferVC: UIViewController {

    //MARK: - Properties
    
  
    var startstop = 0
    var offerid1 = ""
    
    @IBOutlet weak var tableMyOffer: UITableView!
    @IBOutlet weak var buttonAdd: UIButton!
    @IBOutlet weak var viewNoOffer: UIView!
    
    private var arrOffers = [[String: Any]]()
    
    var isRefresh = Bool()
    lazy  var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = Define.APP_COLOR
        return refreshControl
    }()
    
    var isReloadOffer = true
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(newOfferAdded),
                                               name: NSNotification.Name("OfferAdded"),
                                               object: nil)
        
        tableMyOffer.rowHeight = UITableViewAutomaticDimension
        viewNoOffer.isHidden = true
        self.tableMyOffer.addSubview(refreshControl)
        
        if Define.USERDEFAULT.bool(forKey: "isMyOfferReload") {
            Define.USERDEFAULT.set(false, forKey: "isMyOfferReload")
            if !MyModel().isConnectedToInternet() {
                MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET,
                                          alertController: self)
            } else {
                getActiveOffersAPI()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        buttonAdd.layer.cornerRadius = buttonAdd.frame.height / 2
        buttonAdd.clipsToBounds = true
    }
    
    @objc func newOfferAdded() {
        getActiveOffersAPI()
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        isRefresh = true
        refreshControl.beginRefreshing()
        self.getActiveOffersAPI()
        
    }
    
    @IBAction func btnofferpauseplay(_ sender: Any) {
                let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: tableMyOffer)
        let indexPath = tableMyOffer.indexPathForRow(at: buttonPosition) as IndexPath?
        
        let dictData = arrOffers[indexPath!.row]
        
        let offerID = dictData["OfferID"] as! String
        offerid1 = dictData["OfferID"] as! String
        
        let ispause =  dictData["IsPause"] as! String
        
      
        if ispause == "1"

        {
            let alert = UIAlertController(title:"Start", message:"Are you sure you want to Start offer?", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OFFER START", style: .default, handler: {(action) in self.pause()
            }))
            alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
            self.present(alert, animated: true)
            startstop = 1
        }
        else if ispause == "0"
        {
            let alert = UIAlertController(title: "Pause", message:"Are you sure you want to Pause offer?", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OFFER PAUSE", style: .default, handler:{(action) in self.start()
        }))
            alert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        startstop = 0
        }
       print("OfferID: ", offerID)
        
        
        
        
        
    }
    func start() {
        self.playpauseoffer()
    }
    func pause() {
         self.playpauseoffer()
    }
    //MARK: -Button Method
    @IBAction func buttonAdd(_ sender: Any) {
        let addOfferVC = self.storyboard?.instantiateViewController(withIdentifier: "AddNewOfferVC") as! AddNewOfferVC
        addOfferVC.delegate = self
        addOfferVC.isEditOffer = false
        self.present(addOfferVC, animated: true, completion: {
            self.isReloadOffer = true
        })
    }
}

//MARK: - Table View Delegate Method
extension MyOfferVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOffers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let offerCell = tableView.dequeueReusableCell(withIdentifier: "MyOfferTVC") as! MyOfferTVC
        let dictData = arrOffers[indexPath.row]
        
        offerCell.labelOfferName.text = dictData["OfferTitle"] as? String
        offerCell.labelOfferLimit.text = "\(MyModel().dateFormatterStringToString2(date: dictData["StartDate"] as! String)) TO \(MyModel().dateFormatterStringToString2(date: dictData["EndDate"] as! String))"
        offerCell.labelOfferDetail.text = dictData["Description"] as? String
        let strOfferLimit = "Remaining offer \(dictData["RemainingOffer"]!) and Offer limit \(dictData["OfferLimit"]!)."
        offerCell.labelOfferLimitation.text = strOfferLimit
        
        let ispause =  dictData["IsPause"] as! String

        if ispause == "1" {
            if let image = UIImage(named: "play") {
               offerCell.btnplaypause.setImage(image, for:.normal)
            }

        }
        else if ispause == "0"{
            if let image = UIImage(named: "pause") {
                        offerCell.btnplaypause.setImage(image, for:.normal)
            }

        }
        
        
        let arrDiscount = dictData["Kilometer"] as! [[String: Any]]
        
        if arrDiscount.count > 0
        {
            let dictDiscount = getOffer(arrOffers: arrDiscount)
            offerCell.labelDistance.text = "\(dictDiscount!["MinimumKM"] as! String) KM"
            offerCell.labelDiscount.text = "\(dictDiscount!["Discount"] as! String)%"
        }
        
        
        //Set Button Method
        offerCell.buttonOfferEdit.addTarget(self,
                                            action: #selector(self.buttonOfferEdit(_:)),
                                            for: .touchUpInside)
        offerCell.tag = indexPath.row
        
        return offerCell
    }
    
    func getOffer(arrOffers: [[String: Any]]) -> [String: Any]? {
        for dictOffer in arrOffers {
            return dictOffer
        }
        return nil
    }
    
    @objc func buttonOfferEdit(_ sender: UIButton) {
       
        let buttonPosition = sender.convert(CGPoint.zero, to: tableMyOffer)
        let indexPath = tableMyOffer.indexPathForRow(at: buttonPosition) as IndexPath?
        
        let dictData = arrOffers[indexPath!.row]
        
        let offerID = dictData["OfferID"] as! String
        offerid1 = dictData["OfferID"] as! String
        
        print("OfferID: ", offerID)
        getOfferDetail(offerID: offerID)
    }
    
    func getOfferDetail(offerID: String) {
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["OfferID": offerID]
        let strURL = Define.API_BASE_URL + Define.API_GET_OFFER_DETAILS
        print("Parameter: ", parameter, "\nURL: ", strURL)
        
        SwiftAPI().postGetActiveOfferMethod(stringURL: strURL,
                                            userID: Define.USERDEFAULT.value(forKey: "UserID") as! String,
                                            parameter: parameter,
                                            header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            if error != nil {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error!)
                //                MyModel().showAlertMessage(alertTitle: "Alert",
                //                                           alertMessage: Define.ALERT_SERVER,
                //                                           alertController: self)
                self.getOfferDetail(offerID: offerID)
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                
                let status = result!["status_code"] as! Int
                if status == 200 {
                    let arrData = result!["data"] as! [[String: Any]]
                    if arrData.count > 0 {
                        let dictData = arrData[0]
                        let addVC = self.storyboard?.instantiateViewController(withIdentifier: "AddNewOfferVC") as! AddNewOfferVC
                        addVC.isEditOffer = true
                        addVC.dictOfferDetail = dictData
                        addVC.delegate = self
                        self.present(addVC, animated: true, completion: nil)
                    }
                    
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
    
    func playpauseoffer() {
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["OfferID": offerid1,
        "IsPause":startstop]
        let strURL = Define.API_BASE_URL + Define.API_STOPOFFER
        print("Parameter: ", parameter, "\nURL: ", strURL)
        SwiftAPI().postGetActiveOfferMethod(stringURL: strURL,
                                            userID: Define.USERDEFAULT.value(forKey: "UserID") as! String,
                                            parameter: parameter,
                                            header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            if let error = error {
                
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
                //                MyModel().showAlertMessage(alertTitle: "Alert",
                //                                           alertMessage: Define.ALERT_SERVER,
                //                                           alertController: self)
                //self.getOfferDataAPI()
                
                
               
                
            } else {
                
                Define.APPDELEGATE.hideLoadingView()
                print("Result", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                    
                    self.getActiveOffersAPI()
                
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
    
    
}


//MARK: - API
extension MyOfferVC {
    func getActiveOffersAPI() {
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["RestaurantID": Define.USERDEFAULT.value(forKey: "RestaurantID")!]
        let strURL = Define.API_BASE_URL + Define.API_RESTAURANT_ACTIVE_OFFER
        print("Parameter: ", parameter, "\nURL: ", strURL)
        SwiftAPI().postGetActiveOfferMethod(stringURL: strURL,
                                            userID: Define.USERDEFAULT.value(forKey: "UserID")! as! String,
                                            parameter: parameter,
                                            header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result,  error) in
            if let error = error {
                if self.isRefresh {
                    self.isRefresh = false
                    self.refreshControl.endRefreshing()
                }
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.getActiveOffersAPI()
            } else {
                if self.isRefresh {
                    self.isRefresh = false
                    self.refreshControl.endRefreshing()
                }
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ",result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    self.arrOffers = result!["data"] as! [[String: Any]]
                    if self.arrOffers.count > 0 {
                        self.viewNoOffer.isHidden = true
                        self.tableMyOffer.reloadData()
                    } else {
                        self.viewNoOffer.isHidden = false
                    }
                } else {
                    self.viewNoOffer.isHidden = false
                }
            }
        }
    }
}

//MARK: - AddNewOfferDelegate Method
extension MyOfferVC: AddNewOfferDelegate {
    func addNewOfferMethod() {
        dismiss(animated: true, completion: nil)
        self.getActiveOffersAPI()
    }
}
