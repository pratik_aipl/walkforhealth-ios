import UIKit
import GooglePlaces
import SDWebImage

class RestaurantProfileVC: UIViewController {

    //MARK: - Properties
    @IBOutlet var viewNavigation: UIView!
    @IBOutlet weak var buttonEditProfile: UIButton!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var buttonEditProfileImage: UIButton!
    @IBOutlet weak var textRestaurantName: FloatLabelTextField!
    @IBOutlet weak var textOwnerName: FloatLabelTextField!
    @IBOutlet weak var textEmail: FloatLabelTextField!
    @IBOutlet weak var imageEmailDisable: UIImageView!
    @IBOutlet weak var textMobileNo: FloatLabelTextField!
    @IBOutlet weak var imageMobileDisable: UIImageView!
    @IBOutlet weak var textAddress: FloatLabelTextField!
    @IBOutlet weak var textLocality: FloatLabelTextField!
    @IBOutlet weak var textCity: FloatLabelTextField!
    @IBOutlet weak var buttonLocality: UIButton!
    @IBOutlet weak var viewSaveProfile: UIView!
    @IBOutlet weak var textOpeningTime: FloatLabelTextField!
    @IBOutlet weak var textClosingTime: FloatLabelTextField!
    @IBOutlet weak var buttonOpeningTime: UIButton!
    @IBOutlet weak var buttonClosingTime: UIButton!
    
    @IBOutlet weak var constraintViewSaveProfileHeight: NSLayoutConstraint!
    
    var lat = Double()
    var long = Double()
    
    var datePicker: DatePickerDialog? = nil
    
    var imagePicker: UIImagePickerController? = nil
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()
        setUsetInteraction(isSet: false)
        setRestaurantDetail()
        setDatePickerDialog()
        
        lat = Double(StoredData.shared.Latitude ?? "0") ?? 0
        long = Double(StoredData.shared.Longitude ?? "0") ?? 0
        //   textLocality.text = StoredData.shared.CurrentCity
        
//        let updateVC = self.storyboard?.instantiateViewController(withIdentifier: "SetMarkerProfileVC") as! SetMarkerProfileVC
//        updateVC.latitude = self.lat
//        updateVC.longitude = self.long
//        updateVC.delegate = self
//        self.navigationController?.pushViewController(updateVC, animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        self.viewNavigation.backgroundColor = Define.APP_COLOR
        self.viewProfile.backgroundColor = Define.APP_COLOR
        Design().setProfileImage(image: imageProfile)
        self.viewSaveProfile.backgroundColor = Define.APP_COLOR
        self.viewSaveProfile.clipsToBounds = true
        buttonEditProfileImage.layer.cornerRadius = buttonEditProfileImage.frame.height / 2
        buttonEditProfileImage.clipsToBounds = true
        
       
    }
    
    func setDatePickerDialog() {
        datePicker = DatePickerDialog(textColor: Define.APP_COLOR,
                                      buttonColor: Define.BUTTON_COLOR,
                                      font: UIFont(name: "MyriadHebrew-Regular", size: 15.0)!,
                                      showCancelButton: true)
    }
    
    func setUsetInteraction(isSet: Bool) {
        buttonEditProfile.isHidden = isSet
        buttonEditProfile.isUserInteractionEnabled = !isSet
        buttonEditProfileImage.isHidden = !isSet
        buttonEditProfileImage.isUserInteractionEnabled = isSet
        textRestaurantName.isUserInteractionEnabled = isSet
        textOwnerName.isUserInteractionEnabled = isSet
        textEmail.isUserInteractionEnabled = false
        textMobileNo.isUserInteractionEnabled = false
        textAddress.isUserInteractionEnabled = isSet
        textLocality.isUserInteractionEnabled = false
        textCity.isUserInteractionEnabled = isSet
        imageEmailDisable.isHidden = !isSet
        imageMobileDisable.isHidden = !isSet
        buttonLocality.isUserInteractionEnabled = isSet
        buttonOpeningTime.isUserInteractionEnabled = isSet
        buttonClosingTime.isUserInteractionEnabled = isSet
        if isSet {
            labelTitle.text = "Edit Profile"
            constraintViewSaveProfileHeight.constant = 50
        } else {
            labelTitle.text = "Restaurant Profile"
            constraintViewSaveProfileHeight.constant = 0
        }
    }
    
    func setRestaurantDetail() {
        let strImageURL = Define.USERDEFAULT.value(forKey: "Image") as! String
        imageProfile.sd_setImage(with: URL(string: strImageURL))
        textRestaurantName.text = Define.USERDEFAULT.value(forKey: "RestaurantName") as? String
        textOwnerName.text = Define.USERDEFAULT.value(forKey: "PersonName") as? String
        textEmail.text = Define.USERDEFAULT.value(forKey: "EmailID") as? String
        textMobileNo.text = Define.USERDEFAULT.value(forKey: "MobileNo") as? String
        textAddress.text = Define.USERDEFAULT.value(forKey: "Address") as? String
        textLocality.text = Define.USERDEFAULT.value(forKey: "Locality") as? String
        textCity.text = Define.USERDEFAULT.value(forKey: "City") as? String
        if let latitude = Define.USERDEFAULT.value(forKey: "Latitude") as? String {
            lat = Double(latitude)!
        }
        if let longitude = Define.USERDEFAULT.value(forKey: "Longitude") as? String {
            long = Double(longitude)!
        }
        textOpeningTime.text = Define.USERDEFAULT.value(forKey: "OpeningTime") as? String
        textClosingTime.text = Define.USERDEFAULT.value(forKey: "ClosingTime") as? String
        
    }
 
    //MARK: - Button Method
    @IBAction func buttonBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonEditProfile(_ sender: Any) {
        setUsetInteraction(isSet: true)
    }
    
    @IBAction func buttonEditProfileImage(_ sender: Any) {
        imagePicker = UIImagePickerController()
        imagePicker!.delegate = self
        chooseOption()
    }
    
    @IBAction func buttonLocality(_ sender: Any) {
//        let autoCompleteViewController = GMSAutocompleteViewController()
//        autoCompleteViewController.delegate = self
//        self.present(autoCompleteViewController, animated: true, completion: nil)
    }
    
    @IBAction func buttonSaveProfile(_ sender: Any) {
        if textRestaurantName.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Enter Restaurant Name", alertController: self)
        } else if textOwnerName.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Enter Owner Name", alertController: self)
        } else if textAddress.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Enter Address", alertController: self)
        } else if textLocality.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Select Locality", alertController: self)
        } else if textCity.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Enter City", alertController: self)
        } else if !MyModel().isConnectedToInternet() {
            MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET, alertController: self)
        } else {
            self.updateRestaurantAPI()
        }
    }
    @IBAction func buttonOpeningTime(_ sender: Any) {
        datePicker!.show("Select Opening Time",
                         doneButtonTitle: "Done",
                         cancelButtonTitle: "Cencel",
                         defaultDate: MyModel().getTime(strTime: "Open"),
                         minimumDate: nil,
                         maximumDate: nil,
                         datePickerMode: .time)
        { (date) in
            if let selectedDate = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "hh:mm a"
                self.textOpeningTime.text = formatter.string(from: selectedDate)
            }
        }
    }
    
    @IBAction func buttonClosingTime(_ sender: Any) {
        datePicker!.show("Select Closing Time",
                         doneButtonTitle: "Done",
                         cancelButtonTitle: "Cencel",
                         defaultDate: MyModel().getTime(strTime: "Close"),
                         minimumDate: nil,
                         maximumDate: nil,
                         datePickerMode: .time)
        { (date) in
            if let selectedDate = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "hh:mm a"
                self.textClosingTime.text = formatter.string(from: selectedDate)
            }
        }
    }
}

extension RestaurantProfileVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    // MARK: - Set Profile Method.
    func chooseOption() {
        let alertController = UIAlertController (title: nil, message: "Select Option", preferredStyle: .actionSheet)
        let cameraOption = UIAlertAction (title: "Camera", style: .default){
            (action: UIAlertAction) in
            self.fromCamera()
        }
        let photosOption = UIAlertAction (title: "Photos", style: .default){
            (action: UIAlertAction) in
            self.fromPhotos()
        }
        let cancelOption = UIAlertAction (title: "Cancel", style: .cancel)
        
        alertController.addAction(cameraOption)
        alertController.addAction(photosOption)
        alertController.addAction(cancelOption)
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            let popPresenter = alertController.popoverPresentationController
            popPresenter?.sourceView = imageProfile
            popPresenter?.sourceRect = imageProfile.bounds
            self.present(alertController, animated: true, completion: nil)
        }else{
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func fromCamera() {
        imagePicker!.allowsEditing = true
        imagePicker!.sourceType = UIImagePickerControllerSourceType.camera
        self.present(imagePicker!, animated: true, completion: nil)
    }
    
    func fromPhotos() {
        imagePicker!.allowsEditing = true
        imagePicker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(imagePicker!, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
        
        imageProfile.contentMode = .scaleAspectFill
        imageProfile.image = chosenImage
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

//MARK: - Google Delegate Method
//extension RestaurantProfileVC: GMSAutocompleteViewControllerDelegate {
//    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//
//        print("Name : \(place.name)")
//        print("Latitude : \(place.coordinate.latitude)")
//        print("Longitude : \(place.coordinate.longitude)")
//
//        lat = place.coordinate.latitude
//        long = place.coordinate.longitude
//        textLocality.text = place.name
//        dismiss(animated: false, completion: {
//            let updateVC = self.storyboard?.instantiateViewController(withIdentifier: "SetMarkerProfileVC") as! SetMarkerProfileVC
//            updateVC.latitude = self.lat
//            updateVC.longitude = self.long
//            updateVC.delegate = self
//            self.navigationController?.pushViewController(updateVC, animated: true)
//        })
//    }
//
//    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
//        print("Error : \(error.localizedDescription)")
//    }
//
//    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
//        dismiss(animated: true, completion: nil)
//    }
//}

//MARK: - API
extension RestaurantProfileVC {
    func updateRestaurantAPI() {
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["RestaurantID": Define.USERDEFAULT.value(forKey: "RestaurantID")!,
                                        "PersonName": textOwnerName.text!,
                                        "RestaurantName": textRestaurantName.text!,
                                        "Address": textAddress.text!,
                                        "City": textCity.text!,
                                        "Latitude": lat,
                                        "Longitude": long,
                                        "Locality": textLocality.text!,
                                        "add_opning": textOpeningTime.text!,
                                        "closing_time": textClosingTime.text!,
                                        "Restaurant_Image": ""]
        let strURL = Define.API_BASE_URL + Define.API_UPDATE_RESTAURANT
        print("Parameter: ", parameter, "\nURL: ", strURL)
        var imageData: Data?

        if imageProfile.image != nil {
            imageData = UIImageJPEGRepresentation(imageProfile.image!, 0.8)!
        }
        
        SwiftAPI().postImageUplodOfferAndProfile(stringURL: strURL,
                                                 parameters: parameter,
                                                 userID: Define.USERDEFAULT.value(forKey: "UserID") as! String,
                                                 header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String,
                                                 imageName: "Restaurant_Image",
                                                 imageData: imageData)
        { (result, error) in
            if let error = error {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.updateRestaurantAPI()
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    self.setUsetInteraction(isSet: false)
                    MyModel().showTostMessage(alertMessage: "Profile Updated Successfully", alertController: self)
                    let data = result!["data"] as! [String: Any]
                    Define.USERDEFAULT.set(data["RestaurantName"]!, forKey: "RestaurantName")
                    Define.USERDEFAULT.set(data["PersonName"]!, forKey: "PersonName")
                    Define.USERDEFAULT.set(data["Address"]!, forKey: "Address")
                    Define.USERDEFAULT.set(data["City"]!, forKey: "City")
                    Define.USERDEFAULT.set(data["Locality"]!, forKey: "Locality")
                    Define.USERDEFAULT.set(data["Latitude"]!, forKey: "Latitude")
                    Define.USERDEFAULT.set(data["Longitude"]!, forKey: "Longitude")
                    Define.USERDEFAULT.set(data["LogoImgPath"]!, forKey: "Image")
                    Define.USERDEFAULT.set(data["add_opning"]!, forKey: "OpeningTime")
                    Define.USERDEFAULT.set(data["closing_time"]!, forKey: "ClosingTime")
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
}

//MARK: - Set Marker Delegate
extension RestaurantProfileVC: SetMarkerProfileDelegate {
    func getLatLong(lat: Double, long: Double) {
        self.lat = lat
        self.long = long
    }
}
