import UIKit

class UserVC: UIViewController {

   
    //MARK: - Properties
    @IBOutlet weak var tableUser: UITableView!
    @IBOutlet weak var viewNoUser: UIView!
    
    private var arrUsers = [[String: Any]]()
    
    var isRefresh = Bool()
    lazy  var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = Define.APP_COLOR
        return refreshControl
    }()
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()

        tableUser.addSubview(refreshControl)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !MyModel().isConnectedToInternet() {
            MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET, alertController: self)
        } else {
            getActiveUserAPI()
        }
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        isRefresh = true
        refreshControl.beginRefreshing()
        self.getActiveUserAPI()
        
    }
}

//MARK: - TableView Delegate Method
extension UserVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let userCell = tableView.dequeueReusableCell(withIdentifier: "UserTVC") as! UserTVC
        let dictUser = arrUsers[indexPath.row]
        userCell.labelUserName.text = dictUser["Name"] as? String
        userCell.lbloffername.text = dictUser["OfferTitle"] as? String
        userCell.lbldiscount.text = dictUser["Discount"] as? String
        
        if let distance = dictUser["MinimumKM"] as? String {
            userCell.labelDistance.text = "\(distance) KM"
        } else {
            userCell.labelDistance.text = "0 KM"
        }
        
        userCell.labelLimit.text = "\(MyModel().dateFormatterStringToString2(date: dictUser["StartDate"] as! String)) TO \(MyModel().dateFormatterStringToString2(date: dictUser["EndDate"] as! String))"
        
        return userCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dictUser = arrUsers[indexPath.row]
        //let userStatus = dictUser["UserStatus"] as! String
        let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmWalkCompleteVC") as! ConfirmWalkCompleteVC
        detailVC.delegate = self
        detailVC.dictUserData = dictUser
        self.navigationController?.pushViewController(detailVC, animated: true)
//        if userStatus == "Running" {
//            MyModel().showTostMessage(alertMessage: "User still running", alertController: self)
//        } else {
//
//        }
    }
}

//MARK: - API
extension UserVC {
  
    func getActiveUserAPI() {
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["RestaurantID": Define.USERDEFAULT.value(forKey: "RestaurantID")!]
        let strURL = Define.API_BASE_URL + Define.API_ACTIVE_USER
        print("Paramter: ", parameter, "\nURL: ", strURL)
        SwiftAPI().postGetActiveUserMethod(stringURL: strURL,
                                           userID: Define.USERDEFAULT.value(forKey: "UserID") as! String,
                                           parameter: parameter,
                                           header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            if let error = error {
                if self.isRefresh {
                    self.refreshControl.endRefreshing()
                    self.isRefresh = false
                }
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.getActiveUserAPI()
            } else {
                self.arrUsers.removeAll()
                if self.isRefresh {
                    self.refreshControl.endRefreshing()
                    self.isRefresh = false
                }
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    self.arrUsers = result!["data"] as! [[String: Any]]
                    print(self.arrUsers)
                    if self.arrUsers.count > 0 {
                        self.viewNoUser.isHidden = true
                        self.tableUser.reloadData()
                    } else {
                        self.viewNoUser.isHidden = false
                        self.tableUser.reloadData()
                    }
                } else {
                    self.viewNoUser.isHidden = false
                    self.tableUser.reloadData()
                }
            }
        }
    }
}

//MARK: - ConfirmedWalkComplete Delegate Method
extension UserVC: ConfirmWalkCompleteDelegate {
    func promocodeConfirmed() {
        getActiveUserAPI()
    }
}
