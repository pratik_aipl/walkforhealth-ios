import UIKit

class RedeemHistoryVC: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var tableWalker: UITableView!
    @IBOutlet weak var viewNoRedeemedOffer: UIView!
    
    @IBOutlet weak var labelNoRedeemedOffer: UILabel!
    
    private var arrRedeemHistory = [[String: Any]]()
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()

        tableWalker.rowHeight = UITableViewAutomaticDimension
        
        if !MyModel().isConnectedToInternet() {
            MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET,
                                      alertController: self)
        } else {
            getRedeemHistoryAPI()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        self.viewNavigation.backgroundColor = Define.APP_COLOR
        
    }
    
    //MARK: - Button Method
    
    @IBAction func buttonBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func buttonRedeem(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tableWalker)
        let indexPath = tableWalker.indexPathForRow(at: buttonPosition) as IndexPath?
        let dictData = arrRedeemHistory[indexPath!.row]
        self.confirmPromoCodeAPI(data: dictData)
    }
    
}

//MARK: - TableView Delegate Method
extension RedeemHistoryVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRedeemHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RedeemHistoryTVC") as! RedeemHistoryTVC
        //TODO: Set Data
        let dictData = arrRedeemHistory[indexPath.row]
        
        cell.labelWalkerName.text = dictData["Name"] as? String
        
        if let distance = dictData["MinimumKM"] as? String {
            cell.labelWalkingDistance.text = "\(distance) KM"
        } else {
            cell.labelWalkingDistance.text = "0 KM"
        }
        
        if let discount = dictData["Discount"] as? String {
            cell.labelDiscount.text = "\(discount)%"
        } else {
            cell.labelDiscount.text = "0%"
        }
        
        cell.labalWalkingDate.text = dictData["AcceptDate"] as? String
        
        cell.labelOfferName.text = dictData["OfferTitle"] as? String
        cell.labelPromoCode.text = dictData["CouponCode"] as? String
        
        let isPromoCodeApply = dictData["IsPromoApplied"] as! String
        let userStatus = dictData["UserStatus"] as! String
        
        if isPromoCodeApply == "No" && userStatus == "Finished" {
            cell.constraintRedeemViewHeight.constant = 50
        } else {
            cell.constraintRedeemViewHeight.constant = 0
        }
        
        //TODO: Set Button
        cell.buttonRedeem.addTarget(self,
                                    action: #selector(self.buttonRedeem(_:)),
                                    for: .touchUpInside)
        cell.buttonRedeem.tag = indexPath.row
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dictData = arrRedeemHistory[indexPath.row]
        let userStaus = dictData["UserStatus"] as! String
        if userStaus == "Running" {
            MyModel().showTostMessage(alertMessage: "Still Running",
                                      alertController: self)
        } else {
            let restaurantID = dictData[""] as? String
            if restaurantID == nil {
                MyModel().showAlertMessage(alertTitle: "Alert",
                                           alertMessage: "Restaurant Not Found.",
                                           alertController: self)
            } else {
                let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "RedeemHistoryDetailVC") as! RedeemHistoryDetailVC
                detailVC.dictRedeemDetail = dictData
                self.navigationController?.pushViewController(detailVC, animated: true)
            }
        }
    }
}

//MARK: - API
extension RedeemHistoryVC {
    func getRedeemHistoryAPI() {
        self.showLoadingView()
        let parameter: [String: Any] = ["RestaurantID": Define.USERDEFAULT.value(forKey: "RestaurantID")!]
        let strURL = Define.API_BASE_URL + Define.API_USER_OFFER_HISTORY
        print("Parameter: ", parameter, "\nURL: ", strURL)
        SwiftAPI().postGetActiveUserMethod(stringURL: strURL,
                                           userID: Define.USERDEFAULT.value(forKey: "UserID") as! String,
                                           parameter: parameter,
                                           header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            self.arrRedeemHistory.removeAll()
            if let error = error {
                self.hideLoadingView()
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.getRedeemHistoryAPI()
            } else {
                self.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    self.arrRedeemHistory = result!["data"] as! [[String: Any]]
                    self.tableWalker.reloadData()
                    self.viewNoRedeemedOffer.isHidden = true
                } else {
//                    MyModel().showAlertMessage(alertTitle: "Alert",
//                                               alertMessage: result!["message"] as! String,
//                                               alertController: self)
                    self.viewNoRedeemedOffer.isHidden = false
                    self.labelNoRedeemedOffer.text = result!["message"] as? String
                }
            }
        }
    }
    
    func confirmPromoCodeAPI(data: [String: Any]) {
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["WalkerOfferID": data["WalkerOfferID"]!,
                                        "Status": "Yes"]
        let strURL = Define.API_BASE_URL + Define.API_CONFIRM_PROMOCODE
        print("Parameter: ", parameter, "\nURL: ", strURL)
        SwiftAPI().postGetActiveOfferMethod(stringURL: strURL,
                                            userID: Define.USERDEFAULT.value(forKey: "UserID") as! String,
                                            parameter: parameter,
                                            header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            if let error = error {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_INTERNET,
//                                           alertController: self)
                self.confirmPromoCodeAPI(data: data)
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as! Int
                if status == 200 {
                    MyModel().showTostMessage(alertMessage: "Promocode Confirmed", alertController: self)
                    self.getRedeemHistoryAPI()
                } else {
                    MyModel().showAlertMessage(alertTitle: "alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
}

//MARK: - Loading View
extension RedeemHistoryVC {
    func showLoadingView() {
        let view = UIView()
        var activityIndecator = UIActivityIndicatorView()
        
        view.frame = (self.view.bounds)
        view.backgroundColor = UIColor.black
        view.tag = 4444
        view.alpha = 0.5
        
        activityIndecator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activityIndecator.startAnimating()
        
        view.addSubview(activityIndecator)
        activityIndecator.center = view.center
        self.view.addSubview(view)
    }
    
    func hideLoadingView() {
        for view in (self.view.subviews) {
            if view.tag == 4444{
                view.removeFromSuperview()
            }
        }
    }
}
