import UIKit
import IGRPhotoTweaks
import SDWebImage

public protocol AddNewOfferDelegate {
    func addNewOfferMethod()
}

class AddNewOfferVC: UIViewController {

    
    @IBOutlet weak var lblfromtime: UITextField!
    
    @IBOutlet weak var lbltotime: UITextField!
    @IBOutlet weak var OFFEROPENINGTIME: UIButton!
    @IBOutlet weak var OFFERCLOSINGITME: UIButton!
    
    //MARK: - Properties
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var textOfferName: FloatLabelTextField!
    @IBOutlet weak var labelOfferName: UILabel!
    @IBOutlet weak var textOfferDetail: FloatLabelTextView!
    @IBOutlet weak var labelOfferDetail: UILabel!
    @IBOutlet weak var textFromDate: FloatLabelTextField!
    @IBOutlet weak var textToDate: FloatLabelTextField!
    @IBOutlet weak var buttonSave: UIButton!
    @IBOutlet weak var imageBanner: UIImageView!
    @IBOutlet weak var buttonClose: UIButton!
    
    var delegate: AddNewOfferDelegate?
    
    var datePicker: DatePickerDialog? = nil
    var monthLimit: Date? = nil
    var fromDate = Date()
    var toDate = Date()
    
    var imagePicker: UIImagePickerController? = nil
    
    fileprivate var bannerImage: UIImage!
    fileprivate var selectedImage: UIImage!
    
    var isEditOffer = Bool()
    var dictOfferDetail = [String: Any]()
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()
        setDatePickerDialog()
        
        textOfferName.delegate = self
        textOfferDetail.delegate = self
        imageBanner.isUserInteractionEnabled = false
        
        if isEditOffer {
            textOfferName.text = dictOfferDetail["OfferTitle"] as? String
            textOfferDetail.text = dictOfferDetail["Description"] as? String
            let starttime = convertDateformate_24_to12_andReverse(str_date: (dictOfferDetail["OfferStartTime"] as? String)!, strdateFormat: "h:mm a",inputFormat:"HH:mm")
               let endtime = convertDateformate_24_to12_andReverse(str_date: (dictOfferDetail["OfferEndTime"] as? String)!, strdateFormat: "h:mm a",inputFormat:"HH:mm")
            
            
            lblfromtime.text! = starttime
            lbltotime.text! = endtime
            
            textFromDate.text = dateForamtterStringToString(strDate: dictOfferDetail["StartDate"] as! String)
            textToDate.text = dateForamtterStringToString(strDate: dictOfferDetail["EndDate"] as! String)
            
            fromDate = dateForamtterStringToDate(strDate: dictOfferDetail["StartDate"] as! String)
            toDate = dateForamtterStringToDate(strDate: dictOfferDetail["EndDate"] as! String)
            
            imageBanner.backgroundColor = Define.APP_COLOR
            self.imageBanner.isHidden = false
            self.buttonClose.isHidden = false
            
            let strURL = URL(string: dictOfferDetail["OfferImgPath"] as! String)
            self.imageBanner.sd_setImage(with: strURL)
            self.imageBanner.isUserInteractionEnabled = true
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        self.viewNavigation.backgroundColor = Define.APP_COLOR
        Design().setFixButton(button: buttonSave)
        buttonClose.layer.cornerRadius = buttonClose.frame.height / 2
    }
    
    func dateForamtterStringToString(strDate: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let date = formatter.date(from: strDate)
        
        formatter.dateFormat = "dd MMM yyyy"
        
        return formatter.string(from: date!)
    }
    
    func convertDateformate_24_to12_andReverse (str_date : String , strdateFormat: String, inputFormat: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inputFormat
        dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
        
        let date = dateFormatter.date(from: str_date)
        dateFormatter.dateFormat = strdateFormat
        let datestr = dateFormatter.string(from: date!)
        return datestr
    }
    
    
    
    
    func dateForamtterStringToDate(strDate: String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let date = formatter.date(from: strDate)
        
        return date!
    }
    
    func setDatePickerDialog() {
        var dateComponents = DateComponents()
        dateComponents.month = 24
        monthLimit = Calendar.current.date(byAdding: dateComponents, to: Date())
        datePicker = DatePickerDialog(textColor: Define.APP_COLOR,
                                      buttonColor: Define.BUTTON_COLOR,
                                      font: UIFont(name: "MyriadHebrew-Regular", size: 15.0)!,
                                      showCancelButton: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCrop" {
            let yourCropViewController = segue.destination as! ImageCropVC
            yourCropViewController.image = sender as? UIImage
            yourCropViewController.delegate = self;
        }
    }
    
    //MARK: - Button Method
    @IBAction func buttonFromDate(_ sender: Any) {
        let ysterDayDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())!
        datePicker!.show("Offer From Date",
                         doneButtonTitle: "Done",
                         cancelButtonTitle: "Cencel",
                         defaultDate: Date(),
                         minimumDate: Date(),
                         maximumDate: monthLimit,
                         datePickerMode: .date)
        { (date) in
            if let selectedDate = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd MMM yyyy"
                self.textFromDate.text = formatter.string(from: selectedDate)
                self.fromDate = selectedDate
            }
        }
    }
    @IBAction func buttonToDate(_ sender: Any) {
        if textFromDate.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Select From Date", alertController: self)
        } else {
            let ysterDayDate = Calendar.current.date(byAdding: .day, value: 1, to: self.fromDate)!
            datePicker!.show("Offer To Date",
                             doneButtonTitle: "Done",
                             cancelButtonTitle: "Cencel",
                             defaultDate: ysterDayDate,
                             minimumDate: ysterDayDate,
                             maximumDate: monthLimit,
                             datePickerMode: .date)
            { (date) in
                if let selectedDate = date {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd MMM yyyy"
                    self.textToDate.text = formatter.string(from: selectedDate)
                    self.toDate = selectedDate
                }
            }
        }
        
    }
    
    
    @IBAction func buttonOpeningtime(_ sender: Any) {
        let ysterDayDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())!
        
        datePicker!.show("Select Opening Time",
                         doneButtonTitle: "Done",
                         cancelButtonTitle: "Cencel",
                         defaultDate: Date(),
                         minimumDate: Date(),
                         maximumDate: monthLimit,
                         datePickerMode: .time)
        { (date) in
            if let selectedDate = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "h:mm a"
                formatter.amSymbol = "AM"
                formatter.pmSymbol = "PM"
                self.lblfromtime.text = formatter.string(from: selectedDate)
                self.fromDate = selectedDate
            }
        }
    }
    @IBAction func buttonClosingtime(_ sender: Any) {
        if textFromDate.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Select Closing Time", alertController: self)
        } else {
            let ysterDayDate = Calendar.current.date(byAdding: .day, value: 1, to: self.fromDate)!
            datePicker!.show("Offer To Date",
                             doneButtonTitle: "Done",
                             cancelButtonTitle: "Cencel",
                             defaultDate: ysterDayDate,
                             minimumDate: ysterDayDate,
                             maximumDate: monthLimit,
                             datePickerMode: .time)
            { (date) in
                if let selectedDate = date {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "h:mm a"
                    formatter.amSymbol = "AM"
                    formatter.pmSymbol = "PM"
                    self.lbltotime.text = formatter.string(from: selectedDate)
                    self.toDate = selectedDate
                }
            }
        }
        
    }
    
    
    @IBAction func buttonAddBanner(_ sender: Any) {
        imagePicker = UIImagePickerController()
        imagePicker!.delegate = self
        chooseOption()
    }
    
    @IBAction func buttonSave(_ sender: Any) {
        if textOfferName.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Enter Offer Name", alertController: self)
        } else if textOfferDetail.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Enter Offer Detail", alertController: self)
        } else if textFromDate.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Select From Date", alertController: self)
        } else if textToDate.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Select To Date", alertController: self)
        } else if imageBanner.image == nil && !isEditOffer {
            MyModel().showTostMessage(alertMessage: "Select Image For Banner", alertController: self)
        } else {
            let dictData: [String: Any] = ["OfferName": textOfferName.text!,
                                           "OfferDetail": textOfferDetail.text!,
                                           "FromDate": MyModel().dateFormatterToString(date: fromDate),
                                           "ToDate": MyModel().dateFormatterToString(date: toDate),
                                           "OfferStartTime":lblfromtime.text!,
                                           "OfferEndTime":lbltotime.text!,
                                           "BannerImage": selectedImage]
            
            let addOfferVC = self.storyboard?.instantiateViewController(withIdentifier: "AddOfferVC") as! AddOfferVC
            addOfferVC.dictData = dictData
            addOfferVC.delegate = self
            if isEditOffer {
                addOfferVC.dictOfferDetail = dictOfferDetail
                addOfferVC.isEditOffer = isEditOffer
            }
            present(addOfferVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func buttonBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonClose(_ sender: Any) {
        imageBanner.image = nil
        imageBanner.isHidden = true
        buttonClose.isHidden = true
    }
}

//MARK: - Textfield Delegate Method
extension AddNewOfferVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textOfferName {
            labelOfferName.backgroundColor = UIColor.white
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == textOfferName {
            labelOfferName.backgroundColor = Define.LABEL_DARK_COLOR
        }
    }
}

//MARK: - TextView Delegate Method
extension AddNewOfferVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == textOfferDetail {
            labelOfferDetail.backgroundColor = UIColor.white
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == textOfferDetail {
            labelOfferDetail.backgroundColor = Define.LABEL_DARK_COLOR
        }
    }
}

extension AddNewOfferVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    // MARK: - Set Banner Image Method.
    func chooseOption() {
        let alertController = UIAlertController (title: nil, message: "Select Option", preferredStyle: .actionSheet)
        let cameraOption = UIAlertAction (title: "Camera", style: .default){
            (action: UIAlertAction) in
            self.fromCamera()
        }
        let photosOption = UIAlertAction (title: "Photos", style: .default){
            (action: UIAlertAction) in
            self.fromPhotos()
        }
        let cancelOption = UIAlertAction (title: "Cancel", style: .cancel)
        
        alertController.addAction(cameraOption)
        alertController.addAction(photosOption)
        alertController.addAction(cancelOption)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func fromCamera() {
        imagePicker!.allowsEditing = false
        imagePicker!.sourceType = UIImagePickerControllerSourceType.camera
        self.present(imagePicker!, animated: true, completion: nil)
    }
    
    func fromPhotos() {
        imagePicker!.allowsEditing = false
        imagePicker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(imagePicker!, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        bannerImage = chosenImage
        dismiss(animated: false, completion: nil)
        let yourCropViewController = self.storyboard?.instantiateViewController(withIdentifier: "ImageCropVC") as! ImageCropVC
        yourCropViewController.image = bannerImage
        yourCropViewController.delegate = self;
        present(yourCropViewController, animated: false, completion: nil)
        //dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension AddNewOfferVC: IGRPhotoTweakViewControllerDelegate {
    func photoTweaksController(_ controller: IGRPhotoTweakViewController, didFinishWithCroppedImage croppedImage: UIImage) {
        self.imageBanner.isHidden = false
        self.buttonClose.isHidden = false
        self.imageBanner.image = croppedImage
        self.imageBanner.isUserInteractionEnabled = true
        selectedImage = croppedImage
        dismiss(animated: true, completion: nil)
    }
    
    func photoTweaksControllerDidCancel(_ controller: IGRPhotoTweakViewController) {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - AddOfferDelegate Method
extension AddNewOfferVC: AddOfferDelegate{
    func addOfferMethod() {
        dismiss(animated: false, completion: nil)
        self.delegate?.addNewOfferMethod()
    }
    
    
}


