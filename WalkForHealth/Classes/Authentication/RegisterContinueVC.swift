import UIKit
import CountryPicker

class RegisterContinueVC: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var viewRegister: UIView!
    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var imageNoProfile: UIImageView!
    @IBOutlet weak var viewAgree: UIView!
    @IBOutlet weak var buttonAgree: UIButton!
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var buttonLogin: UIButton!
    @IBOutlet weak var viewSecond: UIView!
    @IBOutlet weak var textEmail: FloatLabelTextField!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var textMobileNo: FloatLabelTextField!
    @IBOutlet weak var labelMobileNo: UILabel!
    @IBOutlet weak var viewThird: UIView!
    @IBOutlet weak var textPassword: FloatLabelTextField!
    @IBOutlet weak var labelPassword: UILabel!
    @IBOutlet weak var textConfirmPassword: FloatLabelTextField!
    @IBOutlet weak var labelConfirmPassword: UILabel!
    
    @IBOutlet weak var labelCountryCode: UILabel!
    
    @IBOutlet weak var constraintViewAgreeHeight: NSLayoutConstraint!
    
    //Variable
    var isSecondStep = Bool()
    var isThirdStep = Bool()
    var isAgree = Bool()
    var isViewLoad = false
    
    var registerType = String()
    var profileImage = UIImage()
    var dictRegisterData = [String: String]()
    
    var isProfileImageSet = Bool()
    
    
    var registerFrom = String()
    var registerID = String()
    var isSocialRegister = Bool()
    var strEmailID = String()
    
    private var pickerCode = CountryPicker()
    private var strCountryCode = String()
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()

        textEmail.delegate = self
        textMobileNo.delegate = self
        textPassword.delegate = self
        textConfirmPassword.delegate = self
        
        viewSecond.backgroundColor = Define.APP_COLOR
        viewThird.backgroundColor = Define.APP_COLOR
        
        viewAgree.isHidden = true
        
        let local = Locale.current
        let code = (local as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
        pickerCode.showPhoneNumbers = true
        pickerCode.countryPickerDelegate = self
        pickerCode.setCountry(code!)
        
        print("Data: ", dictRegisterData)
        
        if isSocialRegister {
            textEmail.text = strEmailID
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        Design().setProfileImage(image: imageUser)
        Design().setFixButton(button: buttonNext)
        
        if isProfileImageSet {
            imageUser.image = profileImage
            imageNoProfile.isHidden = true
        }
        
        if isSocialRegister {
            self.buttonNext.setTitle("SUBMIT", for: .normal)
        } else {
            self.buttonNext.setTitle("NEXT", for: .normal)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !isViewLoad {
            viewThird.center.x += self.view.bounds.width
            viewThird.isHidden = false
            isViewLoad = true
        }
    }
    
    
    //MARK: - Button Method
    @IBAction func buttonAgree(_ sender: Any) {
        if isAgree {
            buttonAgree.setImage(#imageLiteral(resourceName: "ic_uncheck"), for: .normal)
            isAgree = false
        } else {
            buttonAgree.setImage(#imageLiteral(resourceName: "ic_check"), for: .normal)
            isAgree = true
        }
    }
    
    @IBAction func buttonNext(_ sender: Any) {
        
        if !isSecondStep {
            if textEmail.text!.isEmpty {
                MyModel().showTostMessage(alertMessage: "Enter Your Email", alertController: self)
            } else if textMobileNo.text!.isEmpty {
                MyModel().showTostMessage(alertMessage: "Enter Mobile Number", alertController: self)
            } else if !MyModel().isValidEmail(testStr: textEmail.text!) {
                MyModel().showTostMessage(alertMessage: "Enter Proper Email", alertController: self)
            } else if !MyModel().isValidMobileNumber(value: textMobileNo.text!) {
                MyModel().showTostMessage(alertMessage: "Enter Proper Mobile Number", alertController: self)
            } else {
                checkMobileEmailAPI()
            }
            
        } else {
            if textPassword.text!.isEmpty {
                MyModel().showTostMessage(alertMessage: "Enter Password", alertController: self)
            } else if textConfirmPassword.text!.isEmpty {
                MyModel().showTostMessage(alertMessage: "Enter Confirm Password", alertController: self)
            } else if textConfirmPassword.text! != textPassword.text! {
                MyModel().showTostMessage(alertMessage: "Confirm Password Not Match", alertController: self)
            } else if !isAgree {
                MyModel().showTostMessage(alertMessage: "Please Accept Terms & Condition", alertController: self)
            } else if !MyModel().isConnectedToInternet() {
                MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET, alertController: self)
            } else {
                if isProfileImageSet {
                    registerWalkerAPI()
                } else {
                    registerWalkerWithoutProfileAPI()
                }
                
            }
        }
    }
    
    @IBAction func buttonLogin(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func buttonBack(_ sender: UIButton) {
        if isSecondStep {
            UIView.animate(withDuration: 0.5, delay: 0.3, options: [], animations: {
                self.viewThird.center.x += self.view.bounds.width
                self.viewSecond.center.x += self.view.bounds.width
            }, completion: { _ in
                self.isSecondStep = false
                self.buttonNext.setTitle("NEXT", for: .normal)
            })
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func buttonTermsAndCondition(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionVC") as! TermsAndConditionVC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func buttonCountryCode(_ sender: Any) {
        let codeView = CountryCodePicker.instanceFromNib() as! CountryCodePicker
        codeView.delegate = self
        self.view.addSubview(codeView)
        codeView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            codeView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            codeView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
            codeView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            codeView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor)
            ])
    }
    
}

//MARK: - TextField Delegate Method
extension RegisterContinueVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == textEmail {
            labelEmail.backgroundColor = UIColor.white
        } else if textField == textMobileNo {
            labelMobileNo.backgroundColor = UIColor.white
        } else if textField == textPassword {
            labelPassword.backgroundColor = UIColor.white
        } else if textField == textConfirmPassword {
            labelConfirmPassword.backgroundColor = UIColor.white
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == textEmail {
            labelEmail.backgroundColor = Define.LABEL_COLOR
        } else if textField == textMobileNo {
            labelMobileNo.backgroundColor = Define.LABEL_COLOR
        } else if textField == textPassword {
            labelPassword.backgroundColor = Define.LABEL_COLOR
        } else if textField == textConfirmPassword {
            labelConfirmPassword.backgroundColor = Define.LABEL_COLOR
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textMobileNo {
            let maxLength = 12
            let currentString: NSString = textMobileNo.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
}

//MARK: - API
extension RegisterContinueVC {
    //With Profile Image
    func registerWalkerAPI() {
        Define.APPDELEGATE.showLoadingView()
        var parameter: [String: Any] = ["FirstName": dictRegisterData["FirstName"]!,
                                        "LastName": dictRegisterData["LastName"]!,
                                        "City": dictRegisterData["City"]!,
                                        "EmailID": textEmail.text!,
                                        "MobileNo": textMobileNo.text!,
                                        "Password": textPassword.text!,
                                        "ConfirmPassword": textConfirmPassword.text!,
                                        "CountryCode": strCountryCode]
        if isSocialRegister {
            parameter["SocialType"] = registerFrom
            parameter["SocialID"] = registerID
        }
        
        let strURL = Define.API_BASE_URL + Define.API_REGISTER_WALKER
        let imageData: Data = UIImageJPEGRepresentation(profileImage, 0.6)!
        let Header = "Basic V0FMS0lORy1BRE1JTjpBUElAV0FMS0lORyEjJFdFQiQ="
        
        print("Parameter: ", parameter, "\nURL", strURL)
        
        SwiftAPI().postImageUplod(stringURL: strURL,
                                  parameters: parameter,
                                  header: Header,
                                  imageName: "Profile_Image",
                                  imageData: imageData)
        { (result, error) in
            if let error = error {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
                self.registerWalkerAPI()
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    Define.USERDEFAULT.set(self.textEmail.text!, forKey: "UserName")
                    Define.USERDEFAULT.set(self.textPassword.text!, forKey: "Password")
                    let data = result!["data"] as! [String: Any]
                    let otpVC = self.storyboard?.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
                    otpVC.mobileNumber = "\(self.strCountryCode)\(self.textMobileNo.text!)"
                    otpVC.userID = data["UserID"] as! Int
                    if self.isSocialRegister {
                        otpVC.isSocialRegister = true
                        otpVC.registerFrom = self.registerFrom
                        otpVC.registerID = self.registerID
                    }
                    self.navigationController?.pushViewController(otpVC, animated: true)
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
    
    //Without Profile Image
    func registerWalkerWithoutProfileAPI() {
        Define.APPDELEGATE.showLoadingView()
        var parameter: [String: Any] = ["FirstName": dictRegisterData["FirstName"]!,
                                        "LastName": dictRegisterData["LastName"]!,
                                        "City": dictRegisterData["City"]!,
                                        "EmailID": textEmail.text!,
                                        "MobileNo": textMobileNo.text!,
                                        "Password": textPassword.text!,
                                        "ConfirmPassword": textConfirmPassword.text!,
                                        "CountryCode": strCountryCode]
        if isSocialRegister {
            parameter["SocialType"] = registerFrom
            parameter["SocialID"] = registerID
        }
        let strURL = Define.API_BASE_URL + Define.API_REGISTER_WALKER
        print("Parameter: ", parameter, "\nURL", strURL)
        
        let Header = "Basic V0FMS0lORy1BRE1JTjpBUElAV0FMS0lORyEjJFdFQiQ="
        
        SwiftAPI().postMethod(stringURL: strURL,
                              parameters: parameter,
                              header: Header)
        { (result, error) in
            if error != nil {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error!)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.registerWalkerWithoutProfileAPI()
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    Define.USERDEFAULT.set(self.textEmail.text!, forKey: "UserName")
                    Define.USERDEFAULT.set(self.textPassword.text!, forKey: "Password")
                    let data = result!["data"] as! [String: Any]
                    let otpVC = self.storyboard?.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
                    otpVC.mobileNumber = "\(self.strCountryCode)\(self.textMobileNo.text!)"
                    otpVC.userID = data["UserID"] as! Int
                    if self.isSocialRegister {
                        otpVC.isSocialRegister = true
                        otpVC.registerFrom = self.registerFrom
                        otpVC.registerID = self.registerID
                    }
                    self.navigationController?.pushViewController(otpVC, animated: true)
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
    
    func checkMobileEmailAPI() {
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["email": textEmail.text!,
                                        "mobile": textMobileNo.text!]
        let strURL = Define.API_BASE_URL + Define.API_CHECK_EMAIL_MOBILE
        print("Parameter: ", parameter, "\nURL: ", strURL)
        
        let Header = "Basic V0FMS0lORy1BRE1JTjpBUElAV0FMS0lORyEjJFdFQiQ="
        
        SwiftAPI().postMethod(stringURL: strURL,
                              parameters: parameter,
                              header: Header)
        { (result, error) in
            if error != nil {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error!)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.checkMobileEmailAPI()
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as! Int
                if status == 200 {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                } else {
                    
                    if self.isSocialRegister {
                        if self.isProfileImageSet {
                            self.registerWalkerAPI()
                        } else {
                            self.registerWalkerWithoutProfileAPI()
                        }
                    } else {
                        UIView.animate(withDuration: 0.5, delay: 0.3, options: [], animations: {
                            self.viewSecond.center.x -= self.view.bounds.width
                            self.viewThird.center.x -= self.view.bounds.width
                        }, completion: { _ in
                            self.isSecondStep = true
                            self.viewAgree.isHidden = false
                            self.buttonNext.setTitle("SUBMIT", for: .normal)
                        })
                    }
                }
            }
        }
    }
}


//MARK: - Country Code
extension RegisterContinueVC: CountryPickerDelegate {
    func countryPhoneCodePicker(_ picker: CountryPicker,
                                didSelectCountryWithName name: String,
                                countryCode: String,
                                phoneCode: String,
                                flag: UIImage) {
        labelCountryCode.text = "\(countryCode) \(phoneCode)"
        strCountryCode = phoneCode
    }
}

//MARK: - Country Code Picker Delegate method
extension RegisterContinueVC: CountryCodePickerDelegate {
    func getCountryCode(strCode: String, strPhoneCode: String) {
        labelCountryCode.text = strCode
        strCountryCode = strPhoneCode
    }
}
