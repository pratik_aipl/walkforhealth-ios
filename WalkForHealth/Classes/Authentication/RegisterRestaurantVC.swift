import UIKit
import GooglePlaces
import CountryPicker

class RegisterRestaurantVC: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var viewSecond: UIView!
    @IBOutlet weak var viewThird: UIView!
    @IBOutlet weak var viewFourth: UIView!
    
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var imageNoProfile: UIImageView!
    @IBOutlet weak var textEmail: FloatLabelTextField!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var textPhoneNo: FloatLabelTextField!
    @IBOutlet weak var labelPhoneNo: UILabel!
    @IBOutlet weak var textAddress: FloatLabelTextField!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var textLocality: FloatLabelTextField!
    @IBOutlet weak var labelLocality: UILabel!
    @IBOutlet weak var textCity: FloatLabelTextField!
    @IBOutlet weak var labelCity: UILabel!
    @IBOutlet weak var textPassword: FloatLabelTextField!
    @IBOutlet weak var labelPassword: UILabel!
    @IBOutlet weak var textConfirmPassword: FloatLabelTextField!
    @IBOutlet weak var labelConfirmPassword: UILabel!
    @IBOutlet weak var buttonAgree: UIButton!
    
    @IBOutlet weak var labelCountryCode: UILabel!
    @IBOutlet weak var buttonNext: UIButton!
    
    @IBOutlet weak var textOpningTime: FloatLabelTextField!
    @IBOutlet weak var textClosingTime: FloatLabelTextField!
    
    var datePicker: DatePickerDialog? = nil
    
    var profileImage = UIImage()
    var isProfileImageSet = Bool()
    var dictRegisterData = [String: String]()
    
    var registerFrom = String()
    var registerID = String()
    var isSocialRegister = Bool()
    var strEmailID = String()
    
    private var isSecondStep = Bool()
    private var isThirdStep = Bool()
    private var isFourthStep = Bool()
    private var isAgree = Bool()
    private var isLoad = Bool()
    
    private var location = [String: Double]()
    
    private var pickerCode = CountryPicker()
    private var strCountryCode = String()
    
    //MARK: - Default  Method
    override func viewDidLoad() {
        super.viewDidLoad()
        textEmail.delegate = self
        textPhoneNo.delegate = self
        textAddress.delegate = self
        textLocality.delegate = self
        textCity.delegate = self
        textPassword.delegate = self
        textConfirmPassword.delegate = self
        
        let local = Locale.current
        let code = (local as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
        pickerCode.showPhoneNumbers = true
        pickerCode.countryPickerDelegate = self
        pickerCode.setCountry(code!)
        
        print("Data: ", dictRegisterData)
        
        if isSocialRegister {
            textEmail.text = strEmailID
        }
        setDatePickerDialog()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        Design().setFixButton(button: buttonNext)
        Design().setProfileImage(image: imageProfile)
        
        textLocality.text = StoredData.shared.CurrentLocality
          textCity.text = StoredData.shared.CurrentCity
        location["Latitude"] = Double(StoredData.shared.Latitude ?? "0")
        location["Longitude"] = Double(StoredData.shared.Longitude ?? "0")
        
        if isProfileImageSet {
            imageProfile.image = profileImage
            imageNoProfile.isHidden = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !isLoad {
            viewThird.center.x += self.view.bounds.width
            viewFourth.center.x += self.view.bounds.width
            
            viewThird.isHidden = false
            viewFourth.isHidden = false
            isLoad = true
        }
    }
    
    func setDatePickerDialog() {
        datePicker = DatePickerDialog(textColor: Define.APP_COLOR,
                                      buttonColor: Define.BUTTON_COLOR,
                                      font: UIFont(name: "MyriadHebrew-Regular", size: 15.0)!,
                                      showCancelButton: true)
    }
    
    //MARK: - Button Method
    @IBAction func buttonNext(_ sender: Any) {
        
        if !isSecondStep {
            if textEmail.text!.isEmpty {
                MyModel().showTostMessage(alertMessage: "Enter Email ID", alertController: self)
            } else if textPhoneNo.text!.isEmpty {
                MyModel().showTostMessage(alertMessage: "Enter Mobile Number", alertController: self)
            } else if !MyModel().isValidEmail(testStr: textEmail.text!) {
                MyModel().showTostMessage(alertMessage: "Enter Proper Email ID", alertController: self)
            } else if !MyModel().isValidMobileNumber(value: textPhoneNo.text!) {
                MyModel().showTostMessage(alertMessage: "Enter Proper Mobile Number", alertController: self)
            } else if textOpningTime.text!.isEmpty {
                MyModel().showTostMessage(alertMessage: "Select Restaurant Opening Time", alertController: self)
            } else if textClosingTime.text!.isEmpty {
                MyModel().showTostMessage(alertMessage: "Select Restaurant Closing Time", alertController: self)
            } else {
                checkMobileEmailAPI()
                if isSocialRegister {
                    self.buttonNext.setTitle("SUBMIT", for: .normal)
                } else {
                   self.buttonNext.setTitle("NEXT", for: .normal)
                }
            }
        } else if !isThirdStep {
            if textAddress.text!.isEmpty {
                MyModel().showTostMessage(alertMessage: "Enter Address", alertController: self)
            } else if textLocality.text!.isEmpty {
                MyModel().showTostMessage(alertMessage: "Enter Locality", alertController: self)
            } else if textCity.text!.isEmpty {
                MyModel().showTostMessage(alertMessage: "Enter City", alertController: self)
            } else {
                if isSocialRegister {
                    if isProfileImageSet {
                        registerRestaurantAPI()
                    } else {
                        registerWalkerWithoutProfileAPI()
                    }
                } else {
                    UIView.animateKeyframes(withDuration: 0.5, delay: 0.3, options: [], animations: {
                        self.viewThird.center.x -= self.view.bounds.width
                        self.viewFourth.center.x -= self.view.bounds.width
                    }) { _ in
                        self.isThirdStep = true
                        self.buttonNext.setTitle("SUBMIT", for: .normal)
                    }
                }
            }
        } else {
            if textPassword.text!.isEmpty {
                MyModel().showTostMessage(alertMessage: "Enter Password", alertController: self)
            } else if textConfirmPassword.text!.isEmpty {
                MyModel().showTostMessage(alertMessage: "Enter Confirm Password", alertController: self)
            } else if textPassword.text! != textConfirmPassword.text! {
                MyModel().showTostMessage(alertMessage: "Confirm Password Not Match", alertController: self)
            } else if !isAgree {
                MyModel().showTostMessage(alertMessage: "Please Accept Terms & Condition", alertController: self)
            } else if !MyModel().isConnectedToInternet() {
                MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET, alertController: self)
            } else {
                if isProfileImageSet {
                    registerRestaurantAPI()
                } else {
                    registerWalkerWithoutProfileAPI()
                }
            }
        }
        
    }
    
    @IBAction func buttonBack(_ sender: UIButton) {
        if isThirdStep {
            UIView.animateKeyframes(withDuration: 0.5, delay: 0.3, options: [], animations: {
                self.viewFourth.center.x += self.view.bounds.width
                self.viewThird.center.x += self.view.bounds.width
            }) { _ in
                self.isThirdStep = false
                self.buttonNext.setTitle("NEXT", for: .normal)
            }
        } else if isSecondStep {
            UIView.animateKeyframes(withDuration: 0.5, delay: 0.3, options: [], animations: {
                self.viewThird.center.x += self.view.bounds.width
                self.viewSecond.center.x += self.view.bounds.width
            }) { _ in
                self.isSecondStep = false
                self.buttonNext.setTitle("NEXT", for: .normal)
            }
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func buttonLogin(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func buttonLocality(_ sender: Any) {
//        let autoCompleteViewController = GMSAutocompleteViewController()
//        autoCompleteViewController.delegate = self
//        self.present(autoCompleteViewController, animated: true, completion: nil)
    }
    
    @IBAction func buttonAgree(_ sender: Any) {
        if isAgree {
            buttonAgree.setImage(#imageLiteral(resourceName: "ic_uncheck"), for: .normal)
            isAgree = false
        } else {
            buttonAgree.setImage(#imageLiteral(resourceName: "ic_check"), for: .normal)
            isAgree = true
        }
    }
    
    @IBAction func buttonTermsAndCondition(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionVC") as! TermsAndConditionVC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func buttonCountryCode(_ sender: Any) {
        let codeView = CountryCodePicker.instanceFromNib() as! CountryCodePicker
        codeView.delegate = self
        self.view.addSubview(codeView)
        codeView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            codeView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            codeView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
            codeView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            codeView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor)
            ])
    }
    
    @IBAction func buttonOpeningTime(_ sender: Any) {
        datePicker!.show("Select Opening Time",
                         doneButtonTitle: "Done",
                         cancelButtonTitle: "Cencel",
                         defaultDate: MyModel().getTime(strTime: "Open"),
                         minimumDate: nil,
                         maximumDate: nil,
                         datePickerMode: .time)
        { (date) in
            if let selectedDate = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "hh:mm a"
                self.textOpningTime.text = formatter.string(from: selectedDate)
            }
        }
    }
    
    @IBAction func buttonClosingTime(_ sender: Any) {
        datePicker!.show("Select Closing Time",
                         doneButtonTitle: "Done",
                         cancelButtonTitle: "Cencel",
                         defaultDate: MyModel().getTime(strTime: "Close"),
                         minimumDate: nil,
                         maximumDate: nil,
                         datePickerMode: .time)
        { (date) in
            if let selectedDate = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "hh:mm a"
                self.textClosingTime.text = formatter.string(from: selectedDate)
            }
        }
    }
}

//MARK: - Textfield Delegate Method
extension RegisterRestaurantVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == textEmail {
            labelEmail.backgroundColor = UIColor.white
        } else if textField == textPhoneNo {
            labelPhoneNo.backgroundColor = UIColor.white
        } else if textField == textAddress {
            labelAddress.backgroundColor = UIColor.white
        } else if textField == textLocality {
            labelLocality.backgroundColor = UIColor.white
        } else if textField == textCity {
            labelCity.backgroundColor = UIColor.white
        } else if textField == textPassword {
            labelPassword.backgroundColor = UIColor.white
        } else if textField == textConfirmPassword {
            labelConfirmPassword.backgroundColor = UIColor.white
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == textEmail {
            labelEmail.backgroundColor = Define.LABEL_DARK_COLOR
        } else if textField == textPhoneNo {
            labelPhoneNo.backgroundColor = Define.LABEL_DARK_COLOR
        } else if textField == textAddress {
            labelAddress.backgroundColor = Define.LABEL_DARK_COLOR
        } else if textField == textLocality {
            labelLocality.backgroundColor = Define.LABEL_DARK_COLOR
        } else if textField == textCity {
            labelCity.backgroundColor = Define.LABEL_DARK_COLOR
        } else if textField == textPassword {
            labelPassword.backgroundColor = Define.LABEL_DARK_COLOR
        } else if textField == textConfirmPassword {
            labelConfirmPassword.backgroundColor = Define.LABEL_DARK_COLOR
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textPhoneNo {
            let maxLength = 12
            let currentString: NSString = textPhoneNo.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
}

//MARK: - Google Delegate Method
//extension RegisterRestaurantVC: GMSAutocompleteViewControllerDelegate {
//    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//
//        print("Name : \(place.name)")
//        print("Latitude : \(place.coordinate.latitude)")
//        print("Longitude : \(place.coordinate.longitude)")
//
//        textLocality.text = place.name
//        location["Latitude"] = place.coordinate.latitude
//        location["Longitude"] = place.coordinate.longitude
//        dismiss(animated: true, completion: nil)
//    }
//
//    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
//        print("Error : \(error.localizedDescription)")
//    }
//
//    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
//        dismiss(animated: true, completion: nil)
//    }
//}

//MARK: - API
extension RegisterRestaurantVC {
    func registerRestaurantAPI() {
        Define.APPDELEGATE.showLoadingView()
        var parameter: [String: Any] = ["RestaurantName": dictRegisterData["RestaurantName"]!,
                                        "PersonName": dictRegisterData["PersonName"]!,
                                        "EmailID": textEmail.text!,
                                        "MobileNo": textPhoneNo.text!,
                                        "CountryCode": strCountryCode,
                                        "Address": textAddress.text!,
                                        "Locality": textLocality.text!,
                                        "City": textCity.text!,
                                        "Password": textPassword.text!,
                                        "ConfirmPassword": textConfirmPassword.text!,
                                        "add_opning": textOpningTime.text!,
                                        "closing_time": textClosingTime.text!,
                                        "Latitude": location["Latitude"]!,
                                        "Longitude": location["Longitude"]!]
        if isSocialRegister {
            parameter["SocialType"] = registerFrom
            parameter["SocialID"] = registerID
        }
        
        let strURL = Define.API_BASE_URL + Define.API_RESTAURANT_REGISTER
        print("Parameter: ", parameter, "\nURL: ", strURL)
        let Header = "Basic V0FMS0lORy1BRE1JTjpBUElAV0FMS0lORyEjJFdFQiQ="
        let imageData: Data = UIImageJPEGRepresentation(profileImage, 0.6)!
        SwiftAPI().postImageUplod(stringURL: strURL,
                                  parameters: parameter,
                                  header: Header,
                                  imageName: "Restaurant_Image",
                                  imageData: imageData)
        { (result, error) in
            if let error = error {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.registerRestaurantAPI()
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    Define.USERDEFAULT.set(self.textEmail.text!, forKey: "UserName")
                    Define.USERDEFAULT.set(self.textPassword.text!, forKey: "Password")
                    let data = result!["data"] as! [String: Any]
                    let otpVC = self.storyboard?.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
                    otpVC.mobileNumber = "\(self.strCountryCode)\(self.textPhoneNo.text!)"
                    otpVC.userID = data["UserID"] as! Int
                    if self.isSocialRegister {
                        otpVC.isSocialRegister = true
                        otpVC.registerFrom = self.registerFrom
                        otpVC.registerID = self.registerID
                    }
                    self.navigationController?.pushViewController(otpVC, animated: true)
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
    
    //Without Profile Image
    func registerWalkerWithoutProfileAPI() {
        Define.APPDELEGATE.showLoadingView()
        var parameter: [String: Any] = ["RestaurantName": dictRegisterData["RestaurantName"]!,
                                        "PersonName": dictRegisterData["PersonName"]!,
                                        "EmailID": textEmail.text!,
                                        "MobileNo": textPhoneNo.text!,
                                        "CountryCode": strCountryCode,
                                        "Address": textAddress.text!,
                                        "Locality": textLocality.text!,
                                        "City": textCity.text!,
                                        "Password": textPassword.text!,
                                        "ConfirmPassword": textConfirmPassword.text!,
                                        "add_opning": textOpningTime.text!,
                                        "closing_time": textClosingTime.text!,
                                        "Latitude": location["Latitude"]!,
                                        "Longitude": location["Longitude"]!]
        if isSocialRegister {
            parameter["SocialType"] = registerFrom
            parameter["SocialID"] = registerID
        }
        let strURL = Define.API_BASE_URL + Define.API_RESTAURANT_REGISTER
        print("Parameter: ", parameter, "\nURL: ", strURL)
        let Header = "Basic V0FMS0lORy1BRE1JTjpBUElAV0FMS0lORyEjJFdFQiQ="
        
        SwiftAPI().postMethod(stringURL: strURL,
                              parameters: parameter,
                              header: Header)
        { (result, error) in
            if error != nil {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error!)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.registerWalkerWithoutProfileAPI()
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    Define.USERDEFAULT.set(self.textEmail.text!, forKey: "UserName")
                    Define.USERDEFAULT.set(self.textPassword.text!, forKey: "Password")
                    let data = result!["data"] as! [String: Any]
                    let otpVC = self.storyboard?.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
                    otpVC.mobileNumber = "\(self.strCountryCode)\(self.textPhoneNo.text!)"
                    otpVC.userID = data["UserID"] as! Int
                    if self.isSocialRegister {
                        otpVC.isSocialRegister = true
                        otpVC.registerFrom = self.registerFrom
                        otpVC.registerID = self.registerID
                    }
                    self.navigationController?.pushViewController(otpVC, animated: true)
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
    
    func checkMobileEmailAPI() {
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["email": textEmail.text!,
                                        "mobile": textPhoneNo.text!]
        let strURL = Define.API_BASE_URL + Define.API_CHECK_EMAIL_MOBILE
        print("Parameter: ", parameter, "\nURL: ", strURL)
        let Header = "Basic V0FMS0lORy1BRE1JTjpBUElAV0FMS0lORyEjJFdFQiQ="
        
        SwiftAPI().postMethod(stringURL: strURL,
                              parameters: parameter,
                              header: Header)
        { (result, error) in
            if error != nil {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error!)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.checkMobileEmailAPI()
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                } else {
                    UIView.animateKeyframes(withDuration: 0.5, delay: 0.3, options: [], animations: {
                        self.viewSecond.center.x -= self.view.bounds.width
                        self.viewThird.center.x -= self.view.bounds.width
                    }) { _ in
                        self.isSecondStep = true
                    }
                }
            }
        }
    }
}

//MARK: - Country Code
extension RegisterRestaurantVC: CountryPickerDelegate {
    func countryPhoneCodePicker(_ picker: CountryPicker,
                                didSelectCountryWithName name: String,
                                countryCode: String,
                                phoneCode: String,
                                flag: UIImage) {
        labelCountryCode.text = "\(countryCode) \(phoneCode)"
        strCountryCode = phoneCode
    }
}

//MARK: - Country Code Picker Delegate method
extension RegisterRestaurantVC: CountryCodePickerDelegate {
    func getCountryCode(strCode: String, strPhoneCode: String) {
        labelCountryCode.text = strCode
        strCountryCode = strPhoneCode
    }
}
