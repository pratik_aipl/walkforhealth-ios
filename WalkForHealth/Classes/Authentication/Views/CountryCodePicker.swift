import UIKit
import CountryPicker

public protocol CountryCodePickerDelegate {
    func getCountryCode(strCode: String, strPhoneCode: String)
}

class CountryCodePicker: UIView {
    
    @IBOutlet weak var viewCodePicker: UIView!
    @IBOutlet weak var pickerCode: CountryPicker!
    
    var delegate: CountryCodePickerDelegate?
    
    private var strCountryCode = String()
    private var strCountryPhoneCode = String()
    
    //MARK: - Load NibFile
    class func instanceFromNib() -> UIView{
        return UINib(nibName: "CountryCodePicker", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewCodePicker.layer.cornerRadius = 10
        viewCodePicker.clipsToBounds = true
        
        let local = Locale.current
        let code = (local as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
        pickerCode.showPhoneNumbers = true
        pickerCode.countryPickerDelegate = self
        pickerCode.setCountry(code!)
    }
    
    @IBAction func buttonCancel(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    @IBAction func buttonDone(_ sender: Any) {
        self.removeFromSuperview()
        self.delegate?.getCountryCode(strCode: strCountryCode, strPhoneCode: strCountryPhoneCode)
    }
}

extension CountryCodePicker: CountryPickerDelegate {
    func countryPhoneCodePicker(_ picker: CountryPicker,
                                didSelectCountryWithName name: String,
                                countryCode: String,
                                phoneCode: String,
                                flag: UIImage) {
        self.strCountryCode = "\(countryCode) \(phoneCode)"
        self.strCountryPhoneCode = phoneCode
    }
}
