import UIKit
import CoreLocation

class SplashVC: UIViewController {

    private var locationMaeneger: CLLocationManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        determineMyCurrentLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            if MyModel().isAlreadyLogin() {
                if !MyModel().isConnectedToInternet() {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: Define.ALERT_INTERNET,
                                               alertController: self)
                } else {
                    if Define.USERDEFAULT.bool(forKey: "isFacebookLogin") {
                        self.socialLoginAPI(type: "Facebook")
                    } else if Define.USERDEFAULT.bool(forKey: "isGoogleLogin") {
                        self.socialLoginAPI(type: "Google")
                    } else {
                        self.loginAPI()
                    }
                }
            } else {
                let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginNC")
                self.present(nextVC!, animated: true, completion: nil)
            }
        }
    }
    
   
    
    func determineMyCurrentLocation() {
        locationMaeneger = CLLocationManager()
        locationMaeneger!.delegate = self
        locationMaeneger!.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationMaeneger!.requestAlwaysAuthorization()
        locationMaeneger!.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            locationMaeneger!.startUpdatingLocation()
        }
    }
}

extension SplashVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        print("Latitude: ", (location?.coordinate.latitude)!,
              "\nLongitude: ", (location?.coordinate.longitude)!)
    }
}


//MARK: - API
extension SplashVC {
    func loginAPI() {
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["Username": Define.USERDEFAULT.value(forKey: "UserName")!,
                                        "Password": Define.USERDEFAULT.value(forKey: "Password")!,
                                        "DeviceID": Define.USERDEFAULT.value(forKey: "FCMToken")!]
        let strURL = Define.API_BASE_URL + Define.API_LOGIN
        print("Parameter: ", parameter, "\nURL: ", strURL)
        let Header = "Basic V0FMS0lORy1BRE1JTjpBUElAV0FMS0lORyEjJFdFQiQ="
        SwiftAPI().postMethod(stringURL: strURL,
                              parameters: parameter,
                              header: Header)
        { (result, error) in
            if let error = error {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
                let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginNC")
                self.present(nextVC!, animated: true, completion: nil)
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    let userData = result!["data"] as! [String: Any]
                    MyModel().setData(userData: userData)
                    let userType = Define.USERDEFAULT.value(forKey: "UserType") as! String
                    if userType == "User" {
                        let storyBoard = UIStoryboard(name: "User", bundle: nil)
                        let userVC = storyBoard.instantiateViewController(withIdentifier: "UserNC")
                        self.present(userVC, animated: true, completion: nil)
                    } else if userType == "Restaurant" {
                        let storyBoard = UIStoryboard(name: "Restaurant", bundle: nil)
                        let restaurantVC = storyBoard.instantiateViewController(withIdentifier: "RestaurantNC")
                        self.present(restaurantVC, animated: true, completion: nil)
                    }
                } else {
                    let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginNC")
                    self.present(nextVC!, animated: true, completion: nil)
                }
            }
        }
        
    }
    
    func socialLoginAPI(type: String) {
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["SocialID": Define.USERDEFAULT.value(forKey: "SocialID")!,
                                        "DeviceID": Define.USERDEFAULT.value(forKey: "FCMToken")!]
        let strURL = Define.API_BASE_URL + Define.API_SOCIAL_LOGIN
        print("Parameter: ", parameter, "\nURL: ", strURL)
        
        let Header = "Basic V0FMS0lORy1BRE1JTjpBUElAV0FMS0lORyEjJFdFQiQ="
        SwiftAPI().postMethod(stringURL: strURL,
                              parameters: parameter,
                              header: Header)
        { (result, error) in
            if let error = error {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
                let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginNC")
                self.present(nextVC!, animated: true, completion: nil)
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    let userData = result!["data"] as! [String: Any]
                    MyModel().setData(userData: userData)
                    
                    if type == "Facebook" {
                        Define.USERDEFAULT.set(true, forKey: "isFacebookLogin")
                    } else if type == "Google" {
                        Define.USERDEFAULT.set(true, forKey: "isGoogleLogin")
                    }
                    
                    let userType = Define.USERDEFAULT.value(forKey: "UserType") as! String
                    if userType == "User" {
                        let storyBoard = UIStoryboard(name: "User", bundle: nil)
                        let userVC = storyBoard.instantiateViewController(withIdentifier: "UserNC")
                        self.present(userVC, animated: true, completion: nil)
                    } else if userType == "Restaurant" {
                        let storyBoard = UIStoryboard(name: "Restaurant", bundle: nil)
                        let restaurantVC = storyBoard.instantiateViewController(withIdentifier: "RestaurantNC")
                        self.present(restaurantVC, animated: true, completion: nil)
                    }
                } else {
                    let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginNC")
                    self.present(nextVC!, animated: true, completion: nil)
                }
            }
        }
        
    }
}
