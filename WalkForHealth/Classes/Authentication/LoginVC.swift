import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn

class LoginVC: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var textUserName: FloatLabelTextField!
    @IBOutlet weak var textPassword: FloatLabelTextField!
    @IBOutlet weak var buttonLogin: UIButton!
    @IBOutlet weak var buttonFacebook: UIButton!
    @IBOutlet weak var buttonGoogle: UIButton!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var labelPassword: UILabel!
    
    private var strEmailID = String()
    var dictSocialData = [String: Any]()
    
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()

        textUserName.delegate = self
        textPassword.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        textUserName.text = nil
        textPassword.text = nil
    }
    
    override func viewWillLayoutSubviews() {
        Design().setFixButton(button: buttonLogin)
        Design().setCustomButton(button: buttonFacebook)
        Design().setCustomButton(button: buttonGoogle)
    }
    
    //MARK: - Button Method
    @IBAction func buttonForgotPassword(_ sender: Any) {
        let forgotVC = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(forgotVC, animated: true)
    }
    
    @IBAction func buttonLogin(_ sender: Any) {
        if textUserName.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Enter Email ID or Phone Number", alertController: self)
        } else if textPassword.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Enter Password", alertController: self)
        } else if !self.checkUserName() {
            MyModel().showTostMessage(alertMessage: "Enter Proper Email or Phone number", alertController: self)
        } else if !MyModel().isConnectedToInternet(){
            MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET, alertController: self)
        } else {
            loginAPI()
        }
        
    }
    
    func checkUserName() -> Bool {
        if MyModel().isValidEmail(testStr: textUserName.text!) {
            return true
        } else if MyModel().isValidMobileNumber(value: textUserName.text!) {
            return true
        } else {
            return false
        }
    }
    
    @IBAction func buttonRegister(_ sender: Any) {
        let registerVC = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        registerVC.isSocialRegister = false
        self.navigationController?.pushViewController(registerVC, animated: true)
    }
    
    @IBAction func buttonFacebook(_ sender: Any) {
        let fbLoginManeger: FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManeger.logIn(withReadPermissions: ["email"], from: self)
        { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                // if user cancel the login
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData()
                }
            }
        }
    }
    
    @IBAction func buttonGoogle(_ sender: Any) {
        GIDSignIn.sharedInstance().delegate=self
        GIDSignIn.sharedInstance().uiDelegate=self
        GIDSignIn.sharedInstance().signIn()
    }
    
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    print(result!)
                   
                    let dictResult = result as! [String: Any]
                    let fbID = FBSDKAccessToken.current()?.userID
                    self.strEmailID = dictResult["email"] as! String
                    
                    self.dictSocialData = [
                        "UserID": dictResult["id"] as? String ?? "",
                        "FullName": dictResult["name"] as? String ?? "",
                        "FirstName": dictResult["first_name"] as? String ?? "",
                        "LastName": dictResult["last_name"] as? String ?? "",
                        "Email": dictResult["email"] as? String ?? "",
                        "ImageData": dictResult["picture"]!
                    ]
                    print("Social Data: \(self.dictSocialData)")
                    self.socialLoginAPI(id: fbID!, type: "Facebook")
                }
            })
        }
    }
}

//MARK: - Textfield Delegate Method
extension LoginVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textUserName {
            labelUserName.backgroundColor = UIColor.white
        } else if textField == textPassword {
            labelPassword.backgroundColor = UIColor.white
        } else {
            labelUserName.backgroundColor = Define.LABEL_COLOR
            labelPassword.backgroundColor = Define.LABEL_COLOR
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == textUserName {
            labelUserName.backgroundColor = Define.LABEL_COLOR
        } else if textField == textPassword {
            labelPassword.backgroundColor = Define.LABEL_COLOR
        }
    }
}

//MARK: - API
extension LoginVC {
    func loginAPI() {
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["Username": textUserName.text!,
                                        "Password": textPassword.text!,
                                        "DeviceID": Define.USERDEFAULT.value(forKey: "FCMToken")!]
        let strURL = Define.API_BASE_URL + Define.API_LOGIN
        print("Parameter: ", parameter, "\nURL: ", strURL)
        let Header = "Basic V0FMS0lORy1BRE1JTjpBUElAV0FMS0lORyEjJFdFQiQ="
        SwiftAPI().postMethod(stringURL: strURL,
                              parameters: parameter,
                              header: Header)
        { (result, error) in
            if let error = error {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.loginAPI()
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    let userData = result!["data"] as! [String: Any]
                    Define.USERDEFAULT.set(self.textUserName.text!, forKey: "UserName")
                    Define.USERDEFAULT.set(self.textPassword.text!, forKey: "Password")
                    MyModel().setData(userData: userData)
                    let userType = Define.USERDEFAULT.value(forKey: "UserType") as! String
                    if userType == "User" {
                        Define.USERDEFAULT.set(Date(), forKey: "LoginDate")
                        let storyBoard = UIStoryboard(name: "User", bundle: nil)
                        let userVC = storyBoard.instantiateViewController(withIdentifier: "UserNC")
                        self.present(userVC, animated: true, completion: nil)
                    } else if userType == "Restaurant" {
                        let storyBoard = UIStoryboard(name: "Restaurant", bundle: nil)
                        let restaurantVC = storyBoard.instantiateViewController(withIdentifier: "RestaurantNC")
                        self.present(restaurantVC, animated: true, completion: nil)
                    }
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
        
    }
    
    func socialLoginAPI(id: String, type: String) {
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["SocialID": id,
                                        "DeviceID": Define.USERDEFAULT.value(forKey: "FCMToken")!]
        let strURL = Define.API_BASE_URL + Define.API_SOCIAL_LOGIN
        print("Parameter: ", parameter, "\nURL: ", strURL)
        
        let Header = "Basic V0FMS0lORy1BRE1JTjpBUElAV0FMS0lORyEjJFdFQiQ="
        SwiftAPI().postMethod(stringURL: strURL,
                              parameters: parameter,
                              header: Header)
        { (result, error) in
            if let error = error {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
                MyModel().showAlertMessage(alertTitle: "Alert",
                                           alertMessage: Define.ALERT_SERVER,
                                           alertController: self)
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    let userData = result!["data"] as! [String: Any]
                    MyModel().setData(userData: userData)
                    
                    if type == "Facebook" {
                        Define.USERDEFAULT.set(true, forKey: "isFacebookLogin")
                        Define.USERDEFAULT.set(id, forKey: "SocialID")
                    } else if type == "Google" {
                        Define.USERDEFAULT.set(true, forKey: "isGoogleLogin")
                        Define.USERDEFAULT.set(id, forKey: "SocialID")
                    }
                    
                    let userType = Define.USERDEFAULT.value(forKey: "UserType") as! String
                    if userType == "User" {
                        let storyBoard = UIStoryboard(name: "User", bundle: nil)
                        let userVC = storyBoard.instantiateViewController(withIdentifier: "UserNC")
                        self.present(userVC, animated: true, completion: nil)
                    } else if userType == "Restaurant" {
                        let storyBoard = UIStoryboard(name: "Restaurant", bundle: nil)
                        let restaurantVC = storyBoard.instantiateViewController(withIdentifier: "RestaurantNC")
                        self.present(restaurantVC, animated: true, completion: nil)
                    }
                } else if status == 401 {
                    let registerVC = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
                    registerVC.isSocialRegister = true
                    registerVC.registerFrom = type
                    registerVC.strEmailID = self.strEmailID
                    registerVC.registerID = id
                    registerVC.dictSocialData = self.dictSocialData
                    self.navigationController?.pushViewController(registerVC, animated: true)
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
        
    }
}

//MARK: - GOOGLE.
extension LoginVC: GIDSignInUIDelegate, GIDSignInDelegate{
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
    }
    
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
                       withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            let image = user.profile.imageURL(withDimension: 100)
            print("Gmail: ", userId!, "\n Token:", idToken!, fullName!, givenName!, familyName!, email!)
            
            self.dictSocialData = [
                "UserID": userId!,
                "Token": idToken!,
                "FullName": fullName!,
                "FirstName": givenName!,
                "LastName": familyName!,
                "Email": email!,
                "Image": image!
            ]
            print("Social Data: \(self.dictSocialData)")
            self.strEmailID = email!
            self.socialLoginAPI(id: userId!, type: "Google")
        } else {
            print("\(error.localizedDescription)")
        }
    }
}
