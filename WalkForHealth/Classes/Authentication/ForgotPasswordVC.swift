import UIKit

class ForgotPasswordVC: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var textEmail: FloatLabelTextField!
    @IBOutlet weak var buttonSend: UIButton!
    @IBOutlet weak var labelEmail: UILabel!
    
    //MARK: - Default Mehtod
    override func viewDidLoad() {
        super.viewDidLoad()
        textEmail.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        self.viewNavigation.backgroundColor = Define.APP_COLOR
        Design().setFixButton(button: buttonSend)
    }
    //MARK: - Button Method
    @IBAction func buttonBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonSend(_ sender: UIButton) {
        if textEmail.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Enter Email ID",
                                      alertController: self)
        } else if !MyModel().isValidEmail(testStr: textEmail.text!) {
            MyModel().showTostMessage(alertMessage: "Enter Proper Email ID",
                                      alertController: self)
        } else if !MyModel().isConnectedToInternet() {
            MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET,
                                      alertController: self)
        } else {
            self.forgotPasswordAPI()
        }
    }
}

//MARK: - TextView Delegate Method
extension ForgotPasswordVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textEmail {
            labelEmail.backgroundColor = UIColor.white
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == textEmail {
            labelEmail.backgroundColor = Define.LABEL_COLOR
        }
    }
}

//MARK: - API
extension ForgotPasswordVC {
    func forgotPasswordAPI() {
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["EmailID": textEmail.text!]
        let strURL = Define.API_BASE_URL  + Define.API_FORGOT_PASSWORD
        print("Parameter:", parameter, "\nURL: ", strURL)
        SwiftAPI().postMethod(stringURL: strURL,
                              parameters: parameter,
                              header: Define.TOKEN_X_WALKING_API_KEY)
        { (result, error) in
            if error != nil {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error!)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.forgotPasswordAPI()
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ",result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    self.textEmail.text = nil
                    MyModel().showAlertMessage(alertTitle: "",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
}
