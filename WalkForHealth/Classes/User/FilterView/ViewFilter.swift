import UIKit

public protocol ViewFilterDelegate {
    func setFilterSelected(strValue: String)
}
class ViewFilter: UIView {

    lazy var tapper: UITapGestureRecognizer = {
        let tapper = UITapGestureRecognizer()
        tapper.addTarget(self, action: #selector(self.handleTapper(_:)))
        return tapper
    }()
    
    var delegate: ViewFilterDelegate?
    
    //MARK: - Load NibFile
    class func instanceFromNib() -> UIView{
        return UINib(nibName: "ViewFilter", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addGestureRecognizer(tapper)
    }
    
    @objc func handleTapper(_ viewTapper: UITapGestureRecognizer){
        self.removeFromSuperview()
    }
    
    @IBAction func buttonFilterCity(_ sender: Any) {
        self.removeFromSuperview()
        self.delegate?.setFilterSelected(strValue: "City")
    }
    
    @IBAction func buttonFilterRestautant(_ sender: Any) {
        self.removeFromSuperview()
        self.delegate?.setFilterSelected(strValue: "Restaurant")
    }
}
