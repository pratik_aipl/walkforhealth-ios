import UIKit
import SDWebImage
import CoreLocation

class OfferDetailVC: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var imageOfferBanner: UIImageView!
    @IBOutlet weak var labelRestaurantName: UILabel!
    @IBOutlet weak var viewRatting: UIView!
    @IBOutlet weak var labelDiscount: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelMoreLess: UILabel!
    @IBOutlet weak var imageMoreLess: UIImageView!
    @IBOutlet weak var viewOffers: UIView!
    @IBOutlet weak var tableOffers: UITableView!
    @IBOutlet weak var labelOfferDetail: UILabel!
    @IBOutlet weak var labelAaddress: UILabel!
    @IBOutlet weak var buttonCall: UIButton!
    @IBOutlet weak var buttonShare: UIButton!
    @IBOutlet weak var buttonStartWalk: UIButton!
    @IBOutlet weak var labelOfferDates: UILabel!
    @IBOutlet weak var labelRestaurantTime: UILabel!
    @IBOutlet weak var labelRatting: UILabel!
    @IBOutlet weak var labelOfferLeft: UILabel!
    
    @IBOutlet weak var constraintOfferViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var imageStar1: UIImageView!
    @IBOutlet weak var imageStar2: UIImageView!
    @IBOutlet weak var imageStar3: UIImageView!
    @IBOutlet weak var imageStar4: UIImageView!
    @IBOutlet weak var imageStar5: UIImageView!
    
    @IBOutlet weak var labelOfferLimit: UILabel!
    
    var dictOffer = [String: Any]()
    var dictSelectedOffer = [String: Any]()
    private  var arrDiscount = [[String: Any]]()
    private var isMoreClick = Bool()
    
    private var locationMaeneger: CLLocationManager?
    
    private var latitude: Double?
    private var longitude: Double?
    private var isAPIFire = Bool()
    
    var isFromLink = Bool()
    var dictLinkData = [String: String]()
    
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromLink {
            determineMyCurrentLocation()
            print("Link Data: ", dictLinkData)
        } else {
            arrDiscount = dictSelectedOffer["Kilometer"] as! [[String: Any]]
            tableOffers.rowHeight = 40
            constraintOfferViewHeight.constant = 0
            print("Data: ", dictOffer)
            print("Selected Offer: ", dictSelectedOffer)
            setRatting()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        self.viewNavigation.backgroundColor = Define.APP_COLOR
        Design().setButton(button: buttonStartWalk)
        Design().setLabel(label: labelRestaurantName)
        Design().setLabel(label: labelDiscount)
        
        if !isFromLink {
            labelRestaurantName.text = dictOffer["RestaurantName"] as? String
            
            let strDistance = Double(dictOffer["Distance"] as? String ?? "0.0")
            labelDistance.text = "\(String(format: "%.2f", strDistance!)) KM"
            labelAaddress.text = "\(dictOffer["Locality"] as? String ?? "")\n\(dictOffer["Address"] as? String ?? ""), \(dictOffer["City"] as! String)"
            labelOfferDetail.text = dictSelectedOffer["Description"] as? String
            let strValidity = "Offer valid From \(dictSelectedOffer["StartDate"] as? String ?? "") to \(dictSelectedOffer["EndDate"] as? String ?? "")"
            
            labelOfferDates.text = strValidity
            
            let strOpenClose = "Open at \(dictOffer["add_opning"] as? String ?? ""), Close at \(dictOffer["closing_time"] as? String ?? "")"
            
            labelRestaurantTime.text = strOpenClose
            labelRatting.text = "(\(dictOffer["Ratings"]!))"
            if arrDiscount.count > 0 {
                labelDiscount.text = self.getMainOffer(arrOffers: arrDiscount)
            } else {
                labelDiscount.text = ""
            }
            
           // let strOfferLimit = "\(dictSelectedOffer["OfferLimit"]!)"
            let strRemainingOffer = "\(dictSelectedOffer["RemainingOffer"]!)"
            //let offerLimit = Double(strOfferLimit)!
            let remainingOffer = Double(strRemainingOffer)!
            
            
            
            let isActiveOffer = dictSelectedOffer["IsAvailable"] as? Bool ?? false
            if isActiveOffer {
                buttonStartWalk.backgroundColor = Define.APP_COLOR
                buttonStartWalk.setTitle("START WALK", for: .normal)
                labelOfferLimit.isHidden = true
                buttonStartWalk.isEnabled = true
                
                if remainingOffer > 0 {
                    buttonStartWalk.backgroundColor = Define.APP_COLOR
                    buttonStartWalk.setTitle("START WALK", for: .normal)
                    labelOfferLimit.isHidden = true
                    buttonStartWalk.isEnabled = true
                    labelOfferLeft.text = "\(strRemainingOffer) offer Left"
                    
                } else {
                    buttonStartWalk.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                    buttonStartWalk.setTitle("Offer Unavailable", for: .normal)
                    labelOfferLimit.isHidden = false
                    buttonStartWalk.isEnabled = false
                    labelOfferLeft.text = "No offer available"
                }
                
            } else {
                if remainingOffer > 0 {
                    labelOfferLeft.text = "\(strRemainingOffer) offer Left"
                } else {
                    labelOfferLeft.text = "No offer available"
                }
                //buttonStartWalk.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                //buttonStartWalk.setTitle("Offer Unavailable", for: .normal)
                labelOfferLimit.isHidden = true
                buttonStartWalk.isHidden = true
                //labelOfferLimit.text = "This offer not available you..!"
            }
            
            let strImageUrl = dictSelectedOffer["OfferImgPath"] as! String
            let activity = UIActivityIndicatorView(activityIndicatorStyle: .white)
            self.imageOfferBanner.addSubview(activity)
            activity.center = imageOfferBanner.center
            activity.startAnimating()
            imageOfferBanner.sd_setImage(with: URL(string: strImageUrl))
            { (image, error, imageType, url) in
                activity.stopAnimating()
                activity.removeFromSuperview()
            }
        }
    }
    
    func determineMyCurrentLocation() {
        locationMaeneger = CLLocationManager()
        locationMaeneger!.delegate = self
        locationMaeneger!.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationMaeneger!.requestAlwaysAuthorization()
        locationMaeneger!.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            locationMaeneger!.startUpdatingLocation()
        } else {
            MyModel().showAlertMessage(alertTitle: "Location",
                                       alertMessage: "Failed to get your location, Please check your location service",
                                       alertController: self)
        }
    }
    
    func setRatting() {
        
        if let strRate = dictOffer["Ratings"] as? String {
            let rate = Double(strRate)
            
            if rate! >= 1.00 && rate! < 2.00 {
                imageStar1.image = #imageLiteral(resourceName: "ic_fill_star")
                imageStar2.image = #imageLiteral(resourceName: "ic_empty_star")
                imageStar3.image = #imageLiteral(resourceName: "ic_empty_star")
                imageStar4.image = #imageLiteral(resourceName: "ic_empty_star")
                imageStar5.image = #imageLiteral(resourceName: "ic_empty_star")
            } else if rate! >= 2.00 && rate! < 3.00 {
                imageStar1.image = #imageLiteral(resourceName: "ic_fill_star")
                imageStar2.image = #imageLiteral(resourceName: "ic_fill_star")
                imageStar3.image = #imageLiteral(resourceName: "ic_empty_star")
                imageStar4.image = #imageLiteral(resourceName: "ic_empty_star")
                imageStar5.image = #imageLiteral(resourceName: "ic_empty_star")
            } else if rate! >= 3.00 && rate! < 4.00 {
                imageStar1.image = #imageLiteral(resourceName: "ic_fill_star")
                imageStar2.image = #imageLiteral(resourceName: "ic_fill_star")
                imageStar3.image = #imageLiteral(resourceName: "ic_fill_star")
                imageStar4.image = #imageLiteral(resourceName: "ic_empty_star")
                imageStar5.image = #imageLiteral(resourceName: "ic_empty_star")
            } else if rate! >= 4.00 && rate! < 5 {
                imageStar1.image = #imageLiteral(resourceName: "ic_fill_star")
                imageStar2.image = #imageLiteral(resourceName: "ic_fill_star")
                imageStar3.image = #imageLiteral(resourceName: "ic_fill_star")
                imageStar4.image = #imageLiteral(resourceName: "ic_fill_star")
                imageStar5.image = #imageLiteral(resourceName: "ic_empty_star")
            } else if rate! >= 5 {
                imageStar1.image = #imageLiteral(resourceName: "ic_fill_star")
                imageStar2.image = #imageLiteral(resourceName: "ic_fill_star")
                imageStar3.image = #imageLiteral(resourceName: "ic_fill_star")
                imageStar4.image = #imageLiteral(resourceName: "ic_fill_star")
                imageStar5.image = #imageLiteral(resourceName: "ic_fill_star")
            } else {
                imageStar1.image = #imageLiteral(resourceName: "ic_empty_star")
                imageStar2.image = #imageLiteral(resourceName: "ic_empty_star")
                imageStar3.image = #imageLiteral(resourceName: "ic_empty_star")
                imageStar4.image = #imageLiteral(resourceName: "ic_empty_star")
                imageStar5.image = #imageLiteral(resourceName: "ic_empty_star")
            }
        } else {
            imageStar1.image = #imageLiteral(resourceName: "ic_empty_star")
            imageStar2.image = #imageLiteral(resourceName: "ic_empty_star")
            imageStar3.image = #imageLiteral(resourceName: "ic_empty_star")
            imageStar4.image = #imageLiteral(resourceName: "ic_empty_star")
            imageStar5.image = #imageLiteral(resourceName: "ic_empty_star")
        }
        
    }
    
    func getMainOffer(arrOffers: [[String: Any]]) -> String {
        for dictOffer in arrOffers {
            let mainOffer = dictOffer["Main"] as? Bool ?? false
            if mainOffer {
                return "\(dictOffer["Discount"] as? String ?? "0")%"
            }
        }
        for dictOffer in arrOffers {
            return "\(dictOffer["Discount"] as? String ?? "0")%"
        }
        return "0%"
    }
    
    func getMainOfferID(arrOffers: [[String: Any]]) -> String {
        for dictOffer in arrOffers {
            let mainOffer = dictOffer["Main"] as? Bool ?? false
            if mainOffer {
                return "\(dictOffer["KilometerID"] as! String)"
            }
        }
        for dictOffer in arrOffers {
            return "\(dictOffer["KilometerID"] as! String)"
        }
        return ""
    }
    
    //MARK: - Button Method
    @IBAction func buttonBack(_ sender: Any) {
        if isFromLink {
            let storyBoard = UIStoryboard(name: "User", bundle: nil)
            let userVC = storyBoard.instantiateViewController(withIdentifier: "UserNC")
            self.present(userVC, animated: false, completion: {
            })
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func buttonMoreLess(_ sender: Any) {
        if isMoreClick {
            constraintOfferViewHeight.constant = 0
            labelMoreLess.text = "More"
            isMoreClick = false
        } else {
            constraintOfferViewHeight.constant = CGFloat(21 + (arrDiscount.count * 40))
            labelMoreLess.text = "Less"
            isMoreClick = true
        }
    }
    
    @IBAction func buttonCall(_ sender: Any) {
        if let url = URL(string: "tel://\(dictOffer["MobileNo"] as! String)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            }
        }
    }
    
    @IBAction func buttonShare(_ sender: Any) {
        let strShareLink = "\(Define.SHARE_LINK_URL)datalink?RestaurantId=\(dictSelectedOffer["RestaurantID"]!)&OfferID=\(dictSelectedOffer["OfferID"]!)"
        let arrShareItem = [strShareLink]
        let activityViewController = UIActivityViewController(activityItems: arrShareItem,
                                                              applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func buttonStartWalk(_ sender: Any) {
        
        let height = Define.USERDEFAULT.value(forKey: "Height") as! String
        if height.isEmpty {
            Define.USERDEFAULT.set(true, forKey: "isFromDashboard")
            let storyBoard = UIStoryboard(name: "AddHealth", bundle: nil)
            let userVC = storyBoard.instantiateViewController(withIdentifier: "AddHealthNC")
            self.present(userVC, animated: true, completion: {
                
            })
        } else  {
            let infoView = AlertInfoView.instanceFromNib() as! AlertInfoView
            infoView.delegate = self
            self.view.addSubview(infoView)
            infoView.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                infoView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
                infoView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
                infoView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
                infoView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor)
                ])
        }
    }
}

//MARK: - Table View Delegate Method
extension OfferDetailVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDiscount.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let offerCell =  tableView.dequeueReusableCell(withIdentifier: "OfferTVC") as! OfferTVC
        
        let dictData = arrDiscount[indexPath.row]
        
        let formatedString = NSMutableAttributedString()
        formatedString
            .normal("Walk upto ")
            .bold("\(dictData["MinimumKM"] as? String ?? "0") km")
            .normal(" you will get ")
            .bold("(\(dictData["Discount"] as? String ?? "0")%)")
            .normal(" Discount")
        
        offerCell.labelOffers.attributedText = formatedString
        
        return offerCell
    }
}

//MARK: - AlertInfoView Delegate Method.
extension OfferDetailVC: AlertInfoViewDelegate{
    func buttonOk() {
        if !MyModel().isConnectedToInternet() {
            MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET,
                                      alertController: self)
        } else {
            addWalkerOfferAPI()
        }
    }
}

//MARK: - API
extension OfferDetailVC {
    func addWalkerOfferAPI() {
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["OfferID": dictSelectedOffer["OfferID"]!,
                                        "OfferKmID": self.getMainOfferID(arrOffers: arrDiscount),
                                        "RestaurantID": dictSelectedOffer["RestaurantID"]!,
                                        "WalkerID": Define.USERDEFAULT.value(forKey: "WalkerID")!]
        let strURL = Define.API_BASE_URL + Define.API_ADD_WALKER_OFFER
        print("Paremeter: ", parameter, "\nURL: ", strURL)
        
        SwiftAPI().postAddUpdateWalkerOfferMethod(stringURL: strURL,
                                                  userID: Define.USERDEFAULT.value(forKey: "UserID")! as! String,
                                                  parameter: parameter,
                                                  header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            if let error = error {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.addWalkerOfferAPI()
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    let data = result!["data"] as! [String: Any]
                    
                    if self.isFromLink {
                        let countVC = self.storyboard?.instantiateViewController(withIdentifier: "StepCountVC") as! StepCountVC
                        countVC.dictRestaurant = self.dictOffer
                        countVC.walkerOfferID = Int(truncating: data["WalkerOfferID"] as! NSNumber)
                        countVC.isFromLink = true
                        self.present(countVC, animated: true, completion: nil)
                    } else {
                        let countVC = self.storyboard?.instantiateViewController(withIdentifier: "StepCountVC") as! StepCountVC
                        countVC.dictRestaurant = self.dictOffer
                        countVC.walkerOfferID = Int(truncating: data["WalkerOfferID"] as! NSNumber)
                        countVC.isFromLink = false
                        self.navigationController?.pushViewController(countVC, animated: true)
                    }
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
    
    func getOfferDetail() {
        locationMaeneger!.stopUpdatingLocation()
        Define.APPDELEGATE.showLoadingView()
        var url = URLComponents(string: Define.API_BASE_URL + Define.API_RESTAURANT_DETAILS)
        url?.queryItems = [
            URLQueryItem(name: "RestaurantID", value: dictLinkData["RestaurantId"]),
            URLQueryItem(name: "OfferID", value: dictLinkData["OfferID"]),
            URLQueryItem(name: "Latitude", value: String(latitude!)),
            URLQueryItem(name: "Longitude", value: String(longitude!))
        ]
        let strURL = "\(url!)"
        print(strURL)
        
        SwiftAPI().getMethodWithHeader(stringURL: strURL)
        { (result, error) in
            if error != nil {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error!)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.getOfferDetail()
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as! Int
                if status == 200 {
                    let arrData = result!["data"] as! [[String: Any]]
                    if arrData.count > 0 {
                        self.dictOffer = arrData[0]
                        self.setDetailData()
                    } else {
                        MyModel().showAlertMessage(alertTitle: "Alert",
                                                   alertMessage: result!["message"] as! String,
                                                   alertController: self)
                    }
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
    
    func setDetailData() {
        labelRestaurantName.text = dictOffer["RestaurantName"] as? String
        
        let strDistance = Double(dictOffer["Distance"] as? String ?? "0.0")
        labelDistance.text = "\(String(format: "%.2f", strDistance!)) KM"
        
        labelAaddress.text = "\(dictOffer["Locality"] as! String)\n\(dictOffer["Address"] as! String), \(dictOffer["City"] as! String)"
        
        let arrData = dictOffer["Offers"] as! [[String: Any]]
        
        if arrData.count > 0 {
            dictSelectedOffer = arrData[0]
            labelOfferDetail.text = dictSelectedOffer["Description"] as? String
            arrDiscount = dictSelectedOffer["Kilometer"] as! [[String: Any]]
            if arrDiscount.count > 0 {
                labelDiscount.text = self.getMainOffer(arrOffers: arrDiscount)
            } else {
                labelDiscount.text = ""
            }
            
            let strImageUrl = dictSelectedOffer["OfferImgPath"] as! String
            let activity = UIActivityIndicatorView(activityIndicatorStyle: .white)
            self.imageOfferBanner.addSubview(activity)
            activity.center = imageOfferBanner.center
            activity.startAnimating()
            imageOfferBanner.sd_setImage(with: URL(string: strImageUrl))
            { (image, error, imageType, url) in
                activity.stopAnimating()
                activity.removeFromSuperview()
            }
            
            let strValidity = "Offer valid From \(dictSelectedOffer["StartDate"] as? String ?? "") to \(dictSelectedOffer["EndDate"] as? String ?? "")"
            
            labelOfferDates.text = strValidity
            
            let strOpenClose = "Open at \(dictOffer["add_opning"] as? String ?? ""), Close at \(dictOffer["closing_time"] as? String ?? "")"
            
            // let strOfferLimit = "\(dictSelectedOffer["OfferLimit"]!)"
            let strRemainingOffer = "\(dictSelectedOffer["RemainingOffer"]!)"
            //let offerLimit = Double(strOfferLimit)!
            let remainingOffer = Double(strRemainingOffer)!
            
            if remainingOffer > 0 {
                buttonStartWalk.backgroundColor = Define.APP_COLOR
                buttonStartWalk.setTitle("Start Walk", for: .normal)
                labelOfferLimit.isHidden = true
                buttonStartWalk.isEnabled = true
                labelOfferLeft.text = "\(strRemainingOffer) offer Left"
                
                let isActiveOffer = dictSelectedOffer["IsAvailable"] as? Bool ?? false
                if isActiveOffer {
                    buttonStartWalk.backgroundColor = Define.APP_COLOR
                    buttonStartWalk.setTitle("START WALK", for: .normal)
                    labelOfferLimit.isHidden = true
                    buttonStartWalk.isEnabled = true
                } else {
                    buttonStartWalk.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                    buttonStartWalk.setTitle("Offer Unavailable", for: .normal)
                    labelOfferLimit.isHidden = false
                    buttonStartWalk.isEnabled = false
                    labelOfferLimit.text = "This offer not available you..!"
                }
            } else {
                buttonStartWalk.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                buttonStartWalk.setTitle("Offer Unavailable", for: .normal)
                labelOfferLimit.isHidden = false
                buttonStartWalk.isEnabled = false
                labelOfferLeft.text = "No offer available"
            }
            
            labelRestaurantTime.text = strOpenClose
            labelRatting.text = "(\(dictOffer["Ratings"]!))"
            tableOffers.rowHeight = 40
            constraintOfferViewHeight.constant = 0
            print("Data: ", dictOffer)
            print("Selected Offer: ", dictSelectedOffer)
            setRatting()
            tableOffers.reloadData()
        } else {
            MyModel().showAlertMessage(alertTitle: "Alert",
                                       alertMessage: "No Offer Available",
                                       alertController: self)
        }
        
    }
}

//MARK: - Attributed String
extension NSMutableAttributedString{
    @discardableResult func bold(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedStringKey: Any] = [.font: UIFont(name: "MyriadHebrew-Bold",
                                                                 size: 15)!,
                                                   NSAttributedStringKey.foregroundColor: Define.LABEL_DARK_COLOR]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedStringKey: Any] = [.font: UIFont(name: "MyriadHebrew-Regular",
                                                                 size: 14)!,
                                                   NSAttributedStringKey.foregroundColor: Define.LABEL_DARK_COLOR]
        let normalString = NSAttributedString(string: text, attributes: attrs)
        append(normalString)
        
        return self
    }
}

//MARK: - Location
extension OfferDetailVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        print("Latitude: ", (location?.coordinate.latitude)!,
              "\nLongitude: ", (location?.coordinate.longitude)!)
        
        latitude = (location?.coordinate.latitude)!
        longitude = (location?.coordinate.longitude)!
        
        if !isAPIFire && (latitude != nil && longitude != nil) {
            isAPIFire = true
            getOfferDetail()
        }
    }
}


//MARK: - Table View Cell Class
class OfferTVC: UITableViewCell {
    
    @IBOutlet weak var labelOffers: UILabel!
    
}
