import UIKit

class HealthVC: UIViewController {
    
    //MARK: - Properties
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonEditProfile: UIButton!
    @IBOutlet weak var textWeight: FloatLabelTextField!
    @IBOutlet weak var textHeight: FloatLabelTextField!
    @IBOutlet weak var textAlegieticIssues: FloatLabelTextView2!
    @IBOutlet weak var buttonSaveProfile: UIButton!
    
    @IBOutlet weak var constraintSaveProfileButtunHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var buttonDaibetesYes: UIButton!
    @IBOutlet weak var buttonDiabetesNo: UIButton!
    @IBOutlet weak var buttonBloodPressureYes: UIButton!
    @IBOutlet weak var buttonBloodPressureNo: UIButton!
    
    private var isDaibetes = Bool()
    private var isBloodPressure = Bool()
    private var daibetes = 0
    private var bloodPressure = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUserInteraction(isSet: false)
        self.setUserDetail()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        self.viewNavigation.backgroundColor = Define.APP_COLOR
        
        buttonSaveProfile.backgroundColor = Define.APP_COLOR
        buttonSaveProfile.clipsToBounds = true
    }
    
    func setUserDetail() {
        
        textWeight.text = Define.USERDEFAULT.value(forKey: "Weight") as? String
        textHeight.text = Define.USERDEFAULT.value(forKey: "Height") as? String
        textAlegieticIssues.text = Define.USERDEFAULT.value(forKey: "Allergy") as? String
        
        //Daibetes
        let strDaibetes = Define.USERDEFAULT.value(forKey: "Diabetes") as! String
        if strDaibetes == "1" {
            isDaibetes = true
            daibetes = 1
            buttonDaibetesYes.setImage(#imageLiteral(resourceName: "ic_radio_fill"), for: .normal)
            buttonDiabetesNo.setImage(#imageLiteral(resourceName: "ic_radio_empty"), for: .normal)
        } else {
            isDaibetes = false
            daibetes = 0
            buttonDiabetesNo.setImage(#imageLiteral(resourceName: "ic_radio_fill"), for: .normal)
            buttonDaibetesYes.setImage(#imageLiteral(resourceName: "ic_radio_empty"), for: .normal)
        }
        
        //BloodPressure
        let strBloodPressure = Define.USERDEFAULT.value(forKey: "BloodPressure") as! String
        if strBloodPressure == "1" {
            isBloodPressure = true
            bloodPressure = 1
            buttonBloodPressureYes.setImage(#imageLiteral(resourceName: "ic_radio_fill"), for: .normal)
            buttonBloodPressureNo.setImage(#imageLiteral(resourceName: "ic_radio_empty"), for: .normal)
        } else {
            isBloodPressure = false
            bloodPressure = 0
            buttonBloodPressureNo.setImage(#imageLiteral(resourceName: "ic_radio_fill"), for: .normal)
            buttonBloodPressureYes.setImage(#imageLiteral(resourceName: "ic_radio_empty"), for: .normal)
        }
    }
    
    func setUserInteraction(isSet: Bool) {
        buttonEditProfile.isHidden = isSet
        buttonEditProfile.isUserInteractionEnabled = !isSet
        
        textHeight.isUserInteractionEnabled = isSet
        textWeight.isUserInteractionEnabled = isSet
        textAlegieticIssues.isUserInteractionEnabled = isSet
        
        buttonSaveProfile.isUserInteractionEnabled = isSet
        
        buttonDaibetesYes.isUserInteractionEnabled = isSet
        buttonDiabetesNo.isUserInteractionEnabled = isSet
        buttonBloodPressureYes.isUserInteractionEnabled = isSet
        buttonBloodPressureNo.isUserInteractionEnabled = isSet
        
        if isSet {
            labelTitle.text = "Edit Health"
            constraintSaveProfileButtunHeight.constant = 50
        } else {
            labelTitle.text = "Health"
            constraintSaveProfileButtunHeight.constant = 0
        }
    }
    
    
    //MARK: - Button Method
    @IBAction func buttonBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonEditProfil(_ sender: Any) {
        self.setUserInteraction(isSet: true)
    }
    
    @IBAction func buttonSaveProfile(_ sender: Any) {
        if textWeight.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Enter Wieght", alertController: self)
        } else if textHeight.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Enter Height", alertController: self)
        } else if !MyModel().isConnectedToInternet() {
            MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET, alertController: self)
        } else {
            self.updateUserprofileAPI()
        }
    }
    
    @IBAction func buttonDaibetesYes(_ sender: Any) {
        if !isDaibetes {
            isDaibetes = true
            daibetes = 1
            buttonDaibetesYes.setImage(#imageLiteral(resourceName: "ic_radio_fill"), for: .normal)
            buttonDiabetesNo.setImage(#imageLiteral(resourceName: "ic_radio_empty"), for: .normal)
        }
    }
    
    @IBAction func buttonDaibetesNo(_ sender: Any) {
        if isDaibetes {
            isDaibetes = false
            daibetes = 0
            buttonDiabetesNo.setImage(#imageLiteral(resourceName: "ic_radio_fill"), for: .normal)
            buttonDaibetesYes.setImage(#imageLiteral(resourceName: "ic_radio_empty"), for: .normal)
        }
    }
    @IBAction func buttonBloodPressureYes(_ sender: Any) {
        if !isBloodPressure {
            isBloodPressure = true
            bloodPressure = 1
            buttonBloodPressureYes.setImage(#imageLiteral(resourceName: "ic_radio_fill"), for: .normal)
            buttonBloodPressureNo.setImage(#imageLiteral(resourceName: "ic_radio_empty"), for: .normal)
        }
    }
    @IBAction func buttonBloodPressureNo(_ sender: Any) {
        if isBloodPressure {
            isBloodPressure = false
            bloodPressure = 0
            buttonBloodPressureNo.setImage(#imageLiteral(resourceName: "ic_radio_fill"), for: .normal)
            buttonBloodPressureYes.setImage(#imageLiteral(resourceName: "ic_radio_empty"), for: .normal)
        }
    }
    
}

//MARK: - API
extension HealthVC {
    func updateUserprofileAPI() {
        Define.APPDELEGATE.showLoadingView()
        let parameter: [String: Any] = ["WalkerID": Define.USERDEFAULT.value(forKey: "WalkerID")!,
                                        "Weight": textWeight.text!,
                                        "Height": textHeight.text!,
                                        "Diabetes": daibetes,
                                        "BloodPressure": bloodPressure,
                                        "Allergy": textAlegieticIssues.text!
        ]
        let strURL = Define.API_BASE_URL + Define.API_UPDATE_WALKER
        print("Parameter: ", parameter, "\nURL: ", strURL)
        
        
        SwiftAPI().postImageUplodOfferAndProfile(stringURL: strURL,
                                                 parameters: parameter,
                                                 userID: Define.USERDEFAULT.value(forKey: "UserID")! as! String,
                                                 header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String,
                                                 imageName: "Profile_Image",
                                                 imageData: nil)
        { (result, error) in
            if let error = error {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.updateUserprofileAPI()
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    self.setUserInteraction(isSet: false)
                    MyModel().showTostMessage(alertMessage: "Health Updated Successfully",
                                              alertController: self)
                    let data = result!["data"] as! [String: Any]
                    Define.USERDEFAULT.set(data["FirstName"]!, forKey: "FirstName")
                    Define.USERDEFAULT.set(data["LastName"]!, forKey: "LastName")
                    Define.USERDEFAULT.set(data["EmailID"]!, forKey: "EmailID")
                    Define.USERDEFAULT.set(data["MobileNo"]!, forKey: "MobileNo")
                    Define.USERDEFAULT.set(data["Birthdate"]!, forKey: "BirthDate")
                    Define.USERDEFAULT.set(data["Gender"]!, forKey: "Gender")
                    Define.USERDEFAULT.set(data["City"]!, forKey: "City")
                    Define.USERDEFAULT.set(data["ProfileImgPath"]!, forKey: "Image")
                    Define.USERDEFAULT.set(data["Age"]!, forKey: "Age")
                    
                    //Health
                    Define.USERDEFAULT.set(data["Weight"]!, forKey: "Weight")
                    Define.USERDEFAULT.set(data["Height"]!, forKey: "Height")
                    Define.USERDEFAULT.set(data["Diabetes"]!, forKey: "Diabetes")
                    Define.USERDEFAULT.set(data["BloodPressure"]!, forKey: "BloodPressure")
                    Define.USERDEFAULT.set(data["Allergy"]!, forKey: "Allergy")
                } else {
                    MyModel().showAlertMessage(alertTitle: "Alert",
                                               alertMessage: result!["message"] as! String,
                                               alertController: self)
                }
            }
        }
    }
}
