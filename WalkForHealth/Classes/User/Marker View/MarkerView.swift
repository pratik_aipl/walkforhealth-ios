import UIKit

public protocol MarkerViewDelegate{
    func didTapOnCkeckOffer(dictData: NSDictionary)
}

class MarkerView: UIView {

    //MARK: - Properties
    @IBOutlet weak var viewInfo: UIView!
    @IBOutlet weak var LabelRestaurantName: UILabel!
    @IBOutlet weak var labelDiscount: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelLocality: UILabel!
    @IBOutlet weak var buttonCheckOffer: UIButton!
    @IBOutlet weak var imageRestaurant: UIImageView!
    @IBOutlet weak var labelRate: UILabel!
    @IBOutlet weak var labelOpenClose: UILabel!
    
    @IBOutlet weak var imageRate1: UIImageView!
    @IBOutlet weak var imageRate2: UIImageView!
    @IBOutlet weak var imageRate3: UIImageView!
    @IBOutlet weak var imageRate4: UIImageView!
    @IBOutlet weak var imageRate5: UIImageView!
    
    var delegate: MarkerViewDelegate?
    var spotData: NSDictionary?
    //MARK: - Load NibFile
    class func instanceFromNib() -> UIView{
        return UINib(nibName: "MarkerView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
    }
    
    //MARK: - Default Method
    override func awakeFromNib() {
        super.awakeFromNib()
        Design().viewWithShadow(view: viewInfo)
        Design().setLabel(label: LabelRestaurantName)
        Design().setLabel(label: labelDiscount)
        Design().setButton(button: buttonCheckOffer)
        MyModel().setCornerRedious(corner: [.topLeft, .topRight], radius: 8, view: imageRestaurant)
    }
    
    //MARK: - Button Method
    @IBAction func buttonCheckOffer(_ sender: Any) {
        self.delegate?.didTapOnCkeckOffer(dictData: spotData!)
    }
}
