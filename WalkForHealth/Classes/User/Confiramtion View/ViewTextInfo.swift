import UIKit

class ViewTextInfo: UIView {

    @IBOutlet weak var viewEmpty: UIView!
    @IBOutlet weak var labelInfoText: UILabel!
    
    lazy var tapper: UITapGestureRecognizer = {
        let tapper = UITapGestureRecognizer()
        tapper.addTarget(self, action: #selector(self.handleTapper(_:)))
        return tapper
    }()
    
    //MARK: - Load NibFile
    class func instanceFromNib() -> UIView{
        return UINib(nibName: "ViewTextInfo", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewEmpty.addGestureRecognizer(tapper)
    }
    
    @objc func handleTapper(_ viewTapper: UITapGestureRecognizer) {
        self.removeFromSuperview()
    }
    @IBAction func buttonClose(_ sender: Any) {
        self.removeFromSuperview()
    }
}
