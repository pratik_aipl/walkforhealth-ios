import UIKit
import SDWebImage

class SplashBannerView: UIView {
    //MARK: - Properties
    @IBOutlet weak var imageBanner: UIImageView!
    
    var strImage = String()
    
    //MARK: - Load NibFile
    class func instanceFromNib() -> UIView{
        return UINib(nibName: "SplashBannerView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let activity = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        self.imageBanner.addSubview(activity)
        activity.center = self.center
        activity.startAnimating()
        self.imageBanner.sd_setImage(with: URL(string: strImage))
        { (image, error, imageType, url) in
            activity.stopAnimating()
            activity.removeFromSuperview()
        }
        
    }
    
    @IBAction func buttonClose(_ sender: Any) {
        self.removeFromSuperview()
    }
}
