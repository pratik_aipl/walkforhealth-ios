
import UIKit
public protocol AlertInfoViewDelegate {
    func buttonOk()
}
class AlertInfoView: UIView {

    @IBOutlet weak var viewEmptyArea: UIView!
    @IBOutlet weak var viewAction: UIView!
    @IBOutlet weak var labelAlertText: UILabel!
    @IBOutlet weak var buttonOk: UIButton!
    
    var delegate: AlertInfoViewDelegate?
    
    lazy var tapper: UITapGestureRecognizer = {
        let tapper = UITapGestureRecognizer()
        tapper.addTarget(self, action: #selector(self.handleTapper(_:)))
        return tapper
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Design().setButton(button: buttonOk)
        self.viewEmptyArea.addGestureRecognizer(tapper)
    }
    
    //MARK: - Load NibFile
    class func instanceFromNib() -> UIView{
        return UINib(nibName: "AlertInfoView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
    }
    
    @objc func handleTapper(_ viewTapper: UITapGestureRecognizer){
        self.removeFromSuperview()
    }
    @IBAction func buttonOk(_ sender: Any) {
        self.delegate?.buttonOk()
        self.removeFromSuperview()
    }
}
