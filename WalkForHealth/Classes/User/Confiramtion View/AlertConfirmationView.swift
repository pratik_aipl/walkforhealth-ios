import UIKit

public protocol AlertConfirmationViewDelegate{
    func buttonCancel()
    func buttonOK()
}

class AlertConfirmationView: UIView {

    @IBOutlet weak var viewEmptyArea: UIView!
    @IBOutlet weak var viewAlert: UIView!
    @IBOutlet weak var labelQuestion: UILabel!
    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var buttonCancel: UIButton!
    @IBOutlet weak var buttonOk: UIButton!
    
    var delegate: AlertConfirmationViewDelegate?
    
    lazy var tapper: UITapGestureRecognizer = {
        let tapper = UITapGestureRecognizer()
        tapper.addTarget(self, action: #selector(self.handleTapper(_:)))
        return tapper
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Design().setButton(button: buttonOk)
        Design().setButton(button: buttonCancel)
        viewEmptyArea.addGestureRecognizer(tapper)
    }
    
    //MARK: - Load NibFile
    class func instanceFromNib() -> UIView{
        return UINib(nibName: "AlertConfirmationView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
    }
    
    @objc func handleTapper(_ viewTapper: UITapGestureRecognizer) {
        self.removeFromSuperview()
    }
    
    @IBAction func buttonCancel(_ sender: Any) {
        self.delegate?.buttonCancel()
        self.removeFromSuperview()
    }
    
    @IBAction func buttonOk(_ sender: Any) {
        self.delegate?.buttonOK()
        self.removeFromSuperview()
    }
}
