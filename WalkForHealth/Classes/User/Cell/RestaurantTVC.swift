import UIKit

class RestaurantTVC: UITableViewCell {

    @IBOutlet weak var viewReataurants: UIView!
    @IBOutlet weak var buttonCheckOffer: UIButton!
    @IBOutlet weak var labelRestaurantName: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelDiscount: UILabel!
    
    @IBOutlet weak var imageStar1: UIImageView!
    @IBOutlet weak var imageStar2: UIImageView!
    @IBOutlet weak var imageStar3: UIImageView!
    @IBOutlet weak var imageStar4: UIImageView!
    @IBOutlet weak var imageStar5: UIImageView!
    @IBOutlet weak var labelRate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Design().viewWithShadow(view: viewReataurants)
        Design().setButton(button: buttonCheckOffer)
        Design().setLabel(label: labelRestaurantName)
        Design().setLabel(label: labelDiscount)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
