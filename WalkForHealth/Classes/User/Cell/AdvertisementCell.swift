import UIKit

class AdvertisementCell: UITableViewCell {

    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var imageAD: UIImageView!
    @IBOutlet weak var constraintImageHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Design().viewWithShadow(view: viewCell)
        imageAD.layer.cornerRadius = 8
        imageAD.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
