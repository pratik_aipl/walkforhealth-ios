import UIKit

class OfferAvailTVC: UITableViewCell {

    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var labelRestaurantName: UILabel!
    @IBOutlet weak var labelDiscount: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelOfferName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var buttonRatting: UIButton!
    @IBOutlet weak var labelRatting: UILabel!
    
    @IBOutlet weak var imageRate1: UIImageView!
    @IBOutlet weak var imageRate2: UIImageView!
    @IBOutlet weak var imageRate3: UIImageView!
    @IBOutlet weak var imageRate4: UIImageView!
    @IBOutlet weak var imageRate5: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Design().viewWithShadow(view: viewCell)
        Design().setLabel(label: labelRestaurantName)
        Design().setLabel(label: labelOfferName)
        Design().setLabel(label: labelDiscount)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }

}
