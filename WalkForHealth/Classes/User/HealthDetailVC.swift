import UIKit
import HealthKit

struct HealthData {
    var step = Double()
    var date = Date()
    var distance = Double()
}
struct StepData {
    var step = Double()
    var date = Date()
}
class HealthDetailVC: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var tableHealthDetail: UITableView!
    @IBOutlet weak var viewNoData: UIView!
    
    var arrData = [HealthData]()
    
    private var humanWeight = Float()
    private var humanHeight = Float()
    private var humanMeasure = Float()
    private var humanAge = Int()
    private var strRequiredCalories = String()
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableHealthDetail.rowHeight = UITableViewAutomaticDimension
        checkAuthentication()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        self.viewNavigation.backgroundColor = Define.APP_COLOR
        
    }
    
    func setData() {
        humanWeight = Float(Define.USERDEFAULT.value(forKey: "Weight") as! String)!
        humanHeight = Float(Define.USERDEFAULT.value(forKey: "Height") as! String)!
        humanAge = Define.USERDEFAULT.value(forKey: "Age") as! Int
        let gender = Define.USERDEFAULT.value(forKey: "Gender") as! String
        if gender == "Male" {
            humanMeasure = 0.415
        } else if gender == "Female" {
            humanMeasure = 0.413
        } else {
            humanMeasure = 0.414
        }
        strRequiredCalories = MyModel().getRequiredCalories(weight: Double(humanWeight), height: Double(humanHeight), age: humanAge)
    }
    
    //MARK: - Button Method
    @IBAction func buttonBack(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}

//MARK: - TableView Delegate Method
extension HealthDetailVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let healthCell = tableView.dequeueReusableCell(withIdentifier: "HealthDetailTVC") as! HealthDetailTVC
        let dictData = arrData[indexPath.row]
        healthCell.labelDate.text = MyModel().dateFormatterDateToString(date: dictData.date)
        
        let strStep = String(format: "%.0f", dictData.step)
        healthCell.labelStepCount.text = strStep
        
        let distanceMile = Measurement(value: dictData.distance, unit: UnitLength.miles)
        let distanceKM = distanceMile.converted(to: UnitLength.kilometers).value
        print("KM : ", distanceKM)
        let strDistance = String(format: "%.2f", distanceKM as Double)
        healthCell.labelDistance.text = strDistance
        
        let strhumanWeight = Define.USERDEFAULT.value(forKey: "Weight") as! String
        
        if !strhumanWeight.isEmpty {
            let calories = self.getPerStepCalories(weight: Double(self.humanWeight), stepSize: Double(self.humanMeasure))
            let totalBurnCalories = Double(dictData.step) * calories
            print("Total Calories: ", totalBurnCalories)
            let strTotalBurnCalories = String(format: "%.0f", totalBurnCalories)
            healthCell.labelCaloriesBorn.text = "Calories Burn : \(strTotalBurnCalories)"
            healthCell.labelRequireCalories.text = "Required Calories : \(strRequiredCalories)"
        } else {
            healthCell.labelCaloriesBorn.text = "Calories Burn : 0"
            healthCell.labelRequireCalories.text = "Required Calories : 0"
        }
        
        return healthCell
    }
}

//MARK: - Health Data
extension HealthDetailVC {
    func checkAuthentication() {
        let stepQuantityType = HKQuantityType.quantityType(forIdentifier: .stepCount)!
        let distanceQuantityType = HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning)
        
        HKHealthStore().requestAuthorization(toShare: nil,
                                             read: [stepQuantityType, distanceQuantityType!])
        { (success, error) in
            if error != nil {
                print("Error: ", error!)
            } else {
//                self.getTodayDistance(completion: { (distance) in
//                    let distanceMile = Measurement(value: distance, unit: UnitLength.miles)
//                    let distanceKM = distanceMile.converted(to: UnitLength.kilometers).value
//                    print("KM : ", distanceKM)
//                    let strDistance = String(format: "%.2f", distanceKM as Double)
//                    self.setLabelDistance(strDistance: strDistance)
//                })
//                self.getTodayStepCount(completion: { (step) in
//                    let strStep = String(format: "%.0f", step)
//                    self.setLabelStepCount(strStep: strStep)
//                })
                self.getMultipleStepData(completion: { (result) in
                    for dictData in result {
                        var healthData = HealthData()
                        healthData.step = dictData.step
                        healthData.date = dictData.date
                        healthData.distance = 0.0
                        self.arrData.append(healthData)
                    }
                    
                    self.getMultiDistanceData(completion: { (arrDistance) in
                        print("Distance: ", arrDistance)
                        
                        for (index,distance) in arrDistance.enumerated() {
                            var dictData = self.arrData[index]
                            dictData.distance = distance
                            self.arrData[index] = dictData
                        }
                        self.setTableData()
                    })
                    
                })
                
                print("Success: ", success)
            }
        }
    }
    
    func setTableData() {
        DispatchQueue.main.async {
            print("Data: ", self.arrData)
            var arrDemoData = self.arrData
            for (index, dictData) in self.arrData.enumerated() {
                let indexArr = (self.arrData.count - 1) - index
                arrDemoData[indexArr] = dictData
            }
            
            let strWeight = Define.USERDEFAULT.value(forKey: "Weight") as! String
            if !strWeight.isEmpty {
                self.setData()
            }
            
            self.arrData = arrDemoData
            self.tableHealthDetail.reloadData()
            
            if self.arrData.count > 0 {
                self.viewNoData.isHidden = true
            } else {
                self.viewNoData.isHidden = false
            }
        }
    }
    
    func getMultipleStepData(completion: @escaping (_ arrStepDataByDate: [StepData]) -> Void) {
        let healthData = HKHealthStore()
        let stepQuantityType = HKQuantityType.quantityType(forIdentifier: .stepCount)
        let now = Date()
        var startDate = Date()
        if let date = Define.USERDEFAULT.value(forKey: "LoginDate") as? Date {
            startDate = date
        }
        var interval = DateComponents()
        interval.day = 1
        var anchorComponents = Calendar.current.dateComponents([.day, .month, .year], from: now)
        anchorComponents.hour = 0
        let anchorDate = Calendar.current.date(from: anchorComponents)
        let query = HKStatisticsCollectionQuery(quantityType: stepQuantityType!,
                                                quantitySamplePredicate: nil,
                                                options: [.cumulativeSum],
                                                anchorDate: anchorDate!,
                                                intervalComponents: interval)
        query.initialResultsHandler = { _, result, error in
            if error != nil {
                print("Error: ", error!)
            } else {
                guard let results = result else {
                    return
                }
                
               
                var arrStepData = [StepData]()
                
                results.enumerateStatistics(from: startDate, to: now) { statistics, _ in
                    if let sum = statistics.sumQuantity() {
                        let steps = sum.doubleValue(for: HKUnit.count())
                        //print("Amount of steps: \(steps), date: \(statistics.startDate)")
                        var setData = StepData()
                        setData.step = steps
                        setData.date = statistics.startDate
                        arrStepData.append(setData)
                    }
                }
                completion(arrStepData)
            }
        }
        healthData.execute(query)
    }
    
    func getMultiDistanceData(completion: @escaping(_ arrDistanceByDate: [Double]) -> Void) {
        let healthData = HKHealthStore()
        let distanceQuantityType = HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning)
        let now = Date()
        var startDate = Date()
        if let date = Define.USERDEFAULT.value(forKey: "LoginDate") as? Date {
            startDate = date
        }
        var interval = DateComponents()
        interval.day = 1
        var anchorComponents = Calendar.current.dateComponents([.day, .month, .year], from: now)
        anchorComponents.hour = 0
        let anchorDate = Calendar.current.date(from: anchorComponents)
        let query = HKStatisticsCollectionQuery(quantityType: distanceQuantityType!,
                                                quantitySamplePredicate: nil,
                                                options: [.cumulativeSum],
                                                anchorDate: anchorDate!,
                                                intervalComponents: interval)
        query.initialResultsHandler = { _, result, error in
            if error != nil {
                print("Error: ", error!)
            } else {
                guard let results = result else {
                    return
                }
                var arrDistance = [Double]()
                results.enumerateStatistics(from: startDate, to: now) { statistics, _ in
                    if let sum = statistics.sumQuantity() {
                        let distance = sum.doubleValue(for: HKUnit.mile())
                        //print("Amount of steps: \(distance), date: \(statistics.startDate)")
                        arrDistance.append(distance)
                    }
                }
                completion(arrDistance)
            }
        }
        healthData.execute(query)
    }
    
    func getPerStepCalories(weight: Double, stepSize: Double) -> Double{
        
        let footDistance = (Double(humanHeight) * stepSize) / 100
        
        let perMileCalories = ((weight * 2.20) * 0.57)
        
        let estimateStepPerMile = Int(1609.34 / footDistance)
        
        let perStepCalories = perMileCalories / Double(estimateStepPerMile)
        print ("Per Step Calories", perStepCalories)
        
        return perStepCalories
    }
}
