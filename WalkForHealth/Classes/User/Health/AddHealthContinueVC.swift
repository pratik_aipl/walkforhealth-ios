import UIKit

class AddHealthContinueVC: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var buttonMale: UIButton!
    @IBOutlet weak var buttonFemale: UIButton!
    @IBOutlet weak var textDateOfBirth: FloatLabelTextField!
    @IBOutlet weak var buttonFinish: UIButton!
    
    var dictHealthData = [String: String]()
    
    var datePicker: DatePickerDialog? = nil
    var dateOfBirth: Date? = nil
    
    private var isMale = true
    private var isFemale = false
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()

        print("Data: ", dictHealthData)
        setDatePickerDialog()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        viewNavigation.backgroundColor = Define.APP_COLOR
        Design().setFixButton(button: buttonFinish)
        
    }
    
    func setDatePickerDialog() {
        datePicker = DatePickerDialog(textColor: Define.APP_COLOR,
                                      buttonColor: Define.BUTTON_COLOR,
                                      font: UIFont(name: "MyriadHebrew-Regular", size: 15.0)!,
                                      showCancelButton: true)
    }
    
    //MARK: - button Method
    @IBAction func buttonBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonMale(_ sender: Any) {
        if isFemale {
            isMale = true
            isFemale = false
            buttonFemale.setImage(#imageLiteral(resourceName: "ic_uncheck"), for: .normal)
            buttonMale.setImage(#imageLiteral(resourceName: "ic_check"), for: .normal)
        }
    }
    
    @IBAction func buttonFemale(_ sender: Any) {
        if isMale {
            isMale = false
            isFemale = true
            buttonMale.setImage(#imageLiteral(resourceName: "ic_uncheck"), for: .normal)
            buttonFemale.setImage(#imageLiteral(resourceName: "ic_check"), for: .normal)
        }
    }
    
    @IBAction func buttonDateOfBirth(_ sender: Any) {
        datePicker!.show("Select Date Of Birth",
                         doneButtonTitle: "Done",
                         cancelButtonTitle: "Cencel",
                         defaultDate: Date(),
                         minimumDate: nil,
                         maximumDate: Date(),
                         datePickerMode: .date)
        { (date) in
            if let selectedDate = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                self.textDateOfBirth.text = formatter.string(from: selectedDate)
                self.dateOfBirth = selectedDate
            }
        }

    }
    
    @IBAction func buttonFinish(_ sender: Any) {
        if textDateOfBirth.text!.isEmpty {
            MyModel().showTostMessage(alertMessage: "Select Date Of Birth",
                                      alertController: self)
        } else if !MyModel().isConnectedToInternet() {
            MyModel().showTostMessage(alertMessage: Define.ALERT_INTERNET,
                                      alertController: self)
        } else {
            addHealthAPI()
        }
    }
    
    @IBAction func buttonSkip(_ sender: Any) {
        if Define.USERDEFAULT.bool(forKey: "isFromDashboard") {
            dismiss(animated: true, completion: nil)
        } else {
            let storyBoard = UIStoryboard(name: "User", bundle: nil)
            let userVC = storyBoard.instantiateViewController(withIdentifier: "UserNC")
            present(userVC, animated: true, completion: {
                
            })
        }
    }
}

//MARK: - API
extension AddHealthContinueVC {
    func addHealthAPI() {
        Define.APPDELEGATE.showLoadingView()
        var parameter: [String: Any] = ["Weight": dictHealthData["Weight"]!,
                                        "Height": dictHealthData["Height"]!,
                                        "Diabetes": dictHealthData["Daibetes"]!,
                                        "BloodPressure": dictHealthData["BloodPressure"]!,
                                        "Allergy": dictHealthData["AlegieticIssue"]!,
                                        "Birthdate": MyModel().dateFormatterToString(date: dateOfBirth!),
                                        "WalkerID": Define.USERDEFAULT.value(forKey: "WalkerID")!]
        if isMale {
            parameter["Gender"] = "Male"
        } else if isFemale {
            parameter["Gender"] = "Female"
        }
        let strURL = Define.API_BASE_URL + Define.API_ADD_WALKER_HEALTH
        print("Parameter: ", parameter, "\nURL: ", strURL)
        SwiftAPI().postMethod(stringURL: strURL,
                              parameters: parameter,
                              header: Define.USERDEFAULT.value(forKey: "AccessToken") as? String)
        { (result, error) in
            if let error = error {
                Define.APPDELEGATE.hideLoadingView()
                print("Error: ", error)
//                MyModel().showAlertMessage(alertTitle: "Alert",
//                                           alertMessage: Define.ALERT_SERVER,
//                                           alertController: self)
                self.addHealthAPI()
            } else {
                Define.APPDELEGATE.hideLoadingView()
                print("Result: ", result!)
                let status = result!["status_code"] as? Int ?? 0
                if status == 200 {
                    
                    Define.USERDEFAULT.set(self.dictHealthData["Weight"]!, forKey: "Weight")
                    Define.USERDEFAULT.set(self.dictHealthData["Height"]!, forKey: "Height")
                    Define.USERDEFAULT.set(self.dictHealthData["Daibetes"]!, forKey: "Diabetes")
                    Define.USERDEFAULT.set(self.dictHealthData["BloodPressure"]!, forKey: "BloodPressure")
                    Define.USERDEFAULT.set(self.dictHealthData["AlegieticIssue"]!, forKey: "Allergy")
                    Define.USERDEFAULT.set(MyModel().dateFormatterToString(date: self.dateOfBirth!), forKey: "BirthDate")
                    Define.USERDEFAULT.set(parameter["Gender"], forKey: "Gender")
                    
                    if Define.USERDEFAULT.bool(forKey: "isFromDashboard") {
                        
                        let alert = UIAlertController(title: nil,
                                                      message: "Health Details Added Successfully",
                                                      preferredStyle: .alert)
                        self.present(alert, animated: true, completion: nil)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                            self.dismiss(animated: true, completion: {
                                self.dismiss(animated: true, completion: {
                                })
                            })
                        }
                        
                    } else {
                        let alert = UIAlertController(title: nil,
                                                      message: "Health Details Added Successfully",
                                                      preferredStyle: .alert)
                        self.present(alert, animated: true, completion: nil)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                            self.dismiss(animated: true, completion: {
                                let storyBoard = UIStoryboard(name: "User", bundle: nil)
                                let userVC = storyBoard.instantiateViewController(withIdentifier: "UserNC")
                                self.present(userVC, animated: true, completion: {
                                })
                            })
                        }
                    }
                } else {
                    
                }
            }
        }
    }
    
    func showMessage() {
        let alert = UIAlertController(title: nil,
                                      message: "Health Details Added Successfully",
                                      preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
            self.dismiss(animated: true, completion: {
                
            })
        }
    }
}
