import UIKit

class OfferListVC: UIViewController {

    //MARK: - Properties.
    @IBOutlet weak var tableOffers: UITableView!
    @IBOutlet weak var viewNavigation: UIView!
    
    var dictOffer = [String: Any]()
    var arrOfferList = [[String: Any]]()
    
    
    //MARK: - Default Method
    override func viewDidLoad() {
        super.viewDidLoad()
        arrOfferList = dictOffer["Offers"] as! [[String: Any]]
        tableOffers.rowHeight = UITableViewAutomaticDimension
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.backgroundColor = Define.APP_COLOR
        self.viewNavigation.backgroundColor = Define.APP_COLOR
    }
    
    //MARK: - Button Method
    @IBAction func buttonBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func buttonViewOffer(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tableOffers)
        let indexPath = tableOffers.indexPathForRow(at: buttonPosition) as IndexPath?
        
        let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailVC") as! OfferDetailVC
        detailVC.dictOffer = dictOffer
        detailVC.isFromLink = false
        detailVC.dictSelectedOffer = arrOfferList[indexPath!.row]
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
}

//MARK: - TableView Method
extension OfferListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfferList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let offerCell = tableView.dequeueReusableCell(withIdentifier: "OfferListTVC") as! OfferListTVC
        //TODO: SET DATA
        let dictData = arrOfferList[indexPath.row] 
        offerCell.labelOfferName.text = dictData["OfferTitle"] as? String
        offerCell.labelOfferDetail.text = dictData["Description"] as? String
        
        let arrDiscount = dictData["Kilometer"] as! [[String: Any]]
        if arrDiscount.count > 0 {
            offerCell.labelDiscount.text = self.getMainOffer(arrOffers: arrDiscount)
        } else {
            offerCell.labelDiscount.text = ""
        }
        
        //TODO: SET BUTTON
        offerCell.buttonViewOffer.addTarget(self,
                                            action: #selector(self.buttonViewOffer(_:)),
                                            for: .touchUpInside)
        offerCell.buttonViewOffer.tag = indexPath.row
        return offerCell
    }
    
    func getMainOffer(arrOffers: [[String: Any]]) -> String {
        for dictOffer in arrOffers {
            let mainOffer = dictOffer["Main"] as? Bool ?? false
            if mainOffer {
                return "\(dictOffer["Discount"] as? String ?? "0")%"
            }
        }
        for dictOffer in arrOffers {
            return "\(dictOffer["Discount"] as? String ?? "0")%"
        }
        return "0%"
    }
}
