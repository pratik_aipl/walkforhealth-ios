import UIKit
import Alamofire
import FBSDKLoginKit
import GoogleSignIn

class StoredData: NSObject {
    
    static let shared = StoredData()
    var CurrentLocality: String?
    var CurrentCity: String?
     var Latitude: String?
     var Longitude: String?
  
}


class MyModel: NSObject {

    //MARK: - Alert Method.
    func showAlertMessage(alertTitle: String, alertMessage: String, alertController: UIViewController) {
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default)
        alert.addAction(ok)
        alertController.present(alert, animated: true, completion: nil)
    }
    
    func showTostMessage(alertMessage: String, alertController: UIViewController) {
        let alert = UIAlertController(title: nil, message: alertMessage, preferredStyle: .alert)
        alertController.present(alert, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
            alertController.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK: - Check Internet.
    func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    //MARK: - Design Method
    func setCornerRedious(corner: UIRectCorner, radius: CGFloat, view: UIView) {
        let path = UIBezierPath(roundedRect: view.bounds,
                                byRoundingCorners: corner,
                                cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        view.layer.mask = mask
    }
    
    //MARK: - Validation Method.
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func isValidMobileNumber(value: String) -> Bool {
        let PHONE_REGEX = "^[0-9]{10,12}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    //MARK: - Date Formatter
    func dateFormatterToString(date: Date) -> String {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyy-MM-dd"
        return dateFormater.string(from:date)
    }
    
    func dateFormatterStringToString(date: String) -> String {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd"
        let recievedDate = dateFormater.date(from: date)
        dateFormater.dateFormat = "dd-MM-yyyy"
        return dateFormater.string(from: recievedDate!)
    }
    
    func dateFormatterStringToString2(date: String) -> String {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd"
        let recievedDate = dateFormater.date(from: date)
        dateFormater.dateFormat = "MM-dd-yy"
        return dateFormater.string(from: recievedDate!)
    }
    
    func dateFormatterStringToDate(date: String) -> Date {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyy-MM-dd"
        
        return dateFormater.date(from: date)!
    }
    
    func dateFormatterStringToSortString(date: String) -> String {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyy-MM-dd"
        
        let recievedDate = dateFormater.date(from: date)
        
        dateFormater.dateFormat = "dd MMM"
        
        return dateFormater.string(from: recievedDate!)
    }
    
    func dateFormatterDateToString(date: Date) -> String {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "EEEE, MMM d"
        
        return dateFormater.string(from:date)
    }
    
    func getTime(strTime: String) -> Date {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "hh:mm a"
        if strTime == "Open" {
            let strOpeningTime = "10:00 AM"
            return dateFormater.date(from: strOpeningTime)!
        } else {
            let strClosingTime = "11:00 PM"
            return dateFormater.date(from: strClosingTime)!
        }
    }
    
    //MARK: - Saving Data
    func setData(userData: [String: Any]) {
        Define.USERDEFAULT.set(userData["login_token"]!, forKey: "AccessToken")
        Define.USERDEFAULT.set(true, forKey: "IsLogin")
        let dictUserData = userData["user_data"] as! [String: Any]
        let userType = dictUserData["Usertype"] as! String
        
        if userType == "User" {
            self.setWalkerData(walkerData: dictUserData)
        } else if userType == "Restaurant" {
            self.setRestaurantData(restaurantData: dictUserData)
        }
    }
    
    func getDirectionsUrl(LatLngOrigin: String, LatLngDestination: String) -> String{
    
        // Origin of route
        let str_origin = "origin=" + LatLngOrigin
        
        // Destination of route
        let str_dest = "destination=" + LatLngDestination
        
        // Sensor enabled
        let sensor = "sensor=false"
        
        //API Key
        let apikey = "key=AIzaSyCSawe4e-nw47PDP86GvW-DlgaCxi6SF-k"
        
        // Building the parameters to the web service
        let parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + apikey;
        
        // Output format
        let output = "json";
        
        let url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        
        return url;
    }
    
    func setWalkerData(walkerData: [String: Any]) {
        Define.USERDEFAULT.set(walkerData["Usertype"]!, forKey: "UserType")
        Define.USERDEFAULT.set(walkerData["UserID"]!, forKey: "UserID")
        Define.USERDEFAULT.set(walkerData["WalkerID"]!, forKey: "WalkerID")
        Define.USERDEFAULT.set(walkerData["FirstName"]!, forKey: "FirstName")
        Define.USERDEFAULT.set(walkerData["LastName"]!, forKey: "LastName")
        Define.USERDEFAULT.set(walkerData["EmailID"]!, forKey: "EmailID")
        Define.USERDEFAULT.set(walkerData["MobileNo"]!, forKey: "MobileNo")
        Define.USERDEFAULT.set(walkerData["Birthdate"]!, forKey: "BirthDate")
        Define.USERDEFAULT.set(walkerData["Gender"]!, forKey: "Gender")
        Define.USERDEFAULT.set(walkerData["City"]!, forKey: "City")
        Define.USERDEFAULT.set(walkerData["ProfileImgPath"]!, forKey: "Image")
        Define.USERDEFAULT.set(walkerData["ThumbImgPath"]!, forKey: "ThumbImage")
        Define.USERDEFAULT.set(walkerData["Age"]!, forKey: "Age")
        
        //Health
        Define.USERDEFAULT.set(walkerData["Weight"]!, forKey: "Weight")
        Define.USERDEFAULT.set(walkerData["Height"]!, forKey: "Height")
        Define.USERDEFAULT.set(walkerData["Diabetes"]!, forKey: "Diabetes")
        Define.USERDEFAULT.set(walkerData["BloodPressure"]!, forKey: "BloodPressure")
        Define.USERDEFAULT.set(walkerData["Allergy"]!, forKey: "Allergy")
    }
    
    func removeWalkerData()  {
        Define.USERDEFAULT.set(false, forKey: "IsLogin")
        Define.USERDEFAULT.removeObject(forKey: "UserName")
        Define.USERDEFAULT.removeObject(forKey: "Password")
        Define.USERDEFAULT.set(false, forKey: "isFromDashboard")
        Define.USERDEFAULT.removeObject(forKey: "AccessToken")
        Define.USERDEFAULT.removeObject(forKey: "UserType")
        Define.USERDEFAULT.removeObject(forKey: "UserID")
        Define.USERDEFAULT.removeObject(forKey: "WalkerID")
        Define.USERDEFAULT.removeObject(forKey: "FirstName")
        Define.USERDEFAULT.removeObject(forKey: "LastName")
        Define.USERDEFAULT.removeObject(forKey: "EmailID")
        Define.USERDEFAULT.removeObject(forKey: "MobileNo")
        Define.USERDEFAULT.removeObject(forKey: "Gender")
        Define.USERDEFAULT.removeObject(forKey: "City")
        Define.USERDEFAULT.removeObject(forKey: "Image")
        Define.USERDEFAULT.removeObject(forKey: "ThumbImage")
        Define.USERDEFAULT.removeObject(forKey: "BirthDate")
        Define.USERDEFAULT.removeObject(forKey: "Weight")
        Define.USERDEFAULT.removeObject(forKey: "Height")
        Define.USERDEFAULT.removeObject(forKey: "Diabetes")
        Define.USERDEFAULT.removeObject(forKey: "BloodPressure")
        Define.USERDEFAULT.removeObject(forKey: "Allergy")
        Define.USERDEFAULT.removeObject(forKey: "Age")
        Define.USERDEFAULT.removeObject(forKey: "LoginDate")
        
        if Define.USERDEFAULT.bool(forKey: "isFacebookLogin") {
            Define.USERDEFAULT.set(false, forKey: "isFacebookLogin")
            Define.USERDEFAULT.removeObject(forKey: "SocialID")
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
        }
        
        if Define.USERDEFAULT.bool(forKey: "isGoogleLogin") {
            Define.USERDEFAULT.set(false, forKey: "isGoogleLogin")
            Define.USERDEFAULT.removeObject(forKey: "SocialID")
            GIDSignIn.sharedInstance()?.signOut()
        }
    }
    
    func setRestaurantData(restaurantData: [String: Any]) {
        Define.USERDEFAULT.set(restaurantData["Usertype"]!, forKey: "UserType")
        Define.USERDEFAULT.set(restaurantData["UserID"]!, forKey: "UserID")
        Define.USERDEFAULT.set(restaurantData["RestaurantID"]!, forKey: "RestaurantID")
        Define.USERDEFAULT.set(restaurantData["RestaurantName"]!, forKey: "RestaurantName")
        Define.USERDEFAULT.set(restaurantData["PersonName"]!, forKey: "PersonName")
        Define.USERDEFAULT.set(restaurantData["EmailID"]!, forKey: "EmailID")
        Define.USERDEFAULT.set(restaurantData["MobileNo"]!, forKey: "MobileNo")
        Define.USERDEFAULT.set(restaurantData["Address"]!, forKey: "Address")
        Define.USERDEFAULT.set(restaurantData["City"]!, forKey: "City")
        Define.USERDEFAULT.set(restaurantData["Locality"]!, forKey: "Locality")
        Define.USERDEFAULT.set(restaurantData["Latitude"]!, forKey: "Latitude")
        Define.USERDEFAULT.set(restaurantData["Longitude"]!, forKey: "Longitude")
        Define.USERDEFAULT.set(restaurantData["LogoImgPath"]!, forKey: "Image")
        Define.USERDEFAULT.set(restaurantData["ThumbImgPath"]!, forKey: "ThumbImage")
        Define.USERDEFAULT.set(restaurantData["add_opning"]!, forKey: "OpeningTime")
        Define.USERDEFAULT.set(restaurantData["closing_time"]!, forKey: "ClosingTime")
    }
    
    func removeRestaurantData() {
        Define.USERDEFAULT.set(false, forKey: "IsLogin")
        Define.USERDEFAULT.removeObject(forKey: "UserName")
        Define.USERDEFAULT.removeObject(forKey: "Password")
        Define.USERDEFAULT.removeObject(forKey: "AccessToken")
        Define.USERDEFAULT.removeObject(forKey: "UserType")
        Define.USERDEFAULT.removeObject(forKey: "UserID")
        Define.USERDEFAULT.removeObject(forKey: "RestaurantID")
        Define.USERDEFAULT.removeObject(forKey: "RestaurantName")
        Define.USERDEFAULT.removeObject(forKey: "PersonName")
        Define.USERDEFAULT.removeObject(forKey: "EmailID")
        Define.USERDEFAULT.removeObject(forKey: "MobileNo")
        Define.USERDEFAULT.removeObject(forKey: "Address")
        Define.USERDEFAULT.removeObject(forKey: "City")
        Define.USERDEFAULT.removeObject(forKey: "Locality")
        Define.USERDEFAULT.removeObject(forKey: "Latitude")
        Define.USERDEFAULT.removeObject(forKey: "Longitude")
        Define.USERDEFAULT.removeObject(forKey: "Image")
        Define.USERDEFAULT.removeObject(forKey: "ThumbImage")
        Define.USERDEFAULT.removeObject(forKey: "OpeningTime")
        Define.USERDEFAULT.removeObject(forKey: "ClosingTime")
        
        if Define.USERDEFAULT.bool(forKey: "isFacebookLogin") {
            Define.USERDEFAULT.set(false, forKey: "isFacebookLogin")
            Define.USERDEFAULT.removeObject(forKey: "SocialID")
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
        }
        
        if Define.USERDEFAULT.bool(forKey: "isGoogleLogin") {
            Define.USERDEFAULT.set(false, forKey: "isGoogleLogin")
            Define.USERDEFAULT.removeObject(forKey: "SocialID")
            GIDSignIn.sharedInstance()?.signOut()
        }
    }
    
    func isAlreadyLogin() -> Bool {
        let isLogin = Define.USERDEFAULT.bool(forKey: "IsLogin")
        if isLogin {
            return true
        } else {
            return false
        }
    }
    
    //MARK: - Loading View
    func showLoadingView(view: UIView) {
        let view = UIView()
        var activityIndecator = UIActivityIndicatorView()
        
        view.frame = (view.bounds)
        view.backgroundColor = UIColor.black
        view.tag = 4444
        view.alpha = 0.5
        
        activityIndecator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activityIndecator.startAnimating()
        
        view.addSubview(activityIndecator)
        activityIndecator.center = view.center
        view.addSubview(view)
    }
    
    func hideLoadingView(view: UIView) {
        for view in (view.subviews) {
            if view.tag == 4444{
                view.removeFromSuperview()
            }
        }
    }
    
    //MARK: - Calories.
    func getRequiredCalories(weight: Double, height: Double, age: Int) -> String{
        let gender = Define.USERDEFAULT.value(forKey: "Gender") as! String
        if gender == "Male" {
            let bmr = (66.0 + (13.7 * weight)) + ((5.0 * height) - (6.5 * Double(age)))
            let rc = bmr * 1.55
            print("Reuired Calories: ", rc)
            let strRC = String(format: "%.0f", rc)
            return strRC
        } else if gender == "Female" {
            let bmr = (655.0 + (9.6 * weight)) + ((1.8 * height) - (4.8 * Double(age)))
            let rc = bmr * 1.55
            print("Reuired Calories: ", rc)
            let strRC = String(format: "%.0f", rc)
            return strRC
        } else {
            return "0"
        }
    }
}
