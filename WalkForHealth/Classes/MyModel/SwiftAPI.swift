import UIKit
import Alamofire

class SwiftAPI: NSObject {

    // MARK: - GET Function.
    func getMethod(stringURL: String, callBack: @escaping (_ result: Dictionary<String, Any>?, _ error: Error?) -> ()) {
        
        Alamofire.request(stringURL).responseJSON { response in
            switch response.result {
            case .success:
                let dictResult: Dictionary = response.value as! [String : AnyObject]
                callBack(dictResult,nil)
            case .failure(let encodingError):
                callBack(nil, encodingError)
            }
        }
    }
    func getMethodWithHeader(stringURL: String, callBack: @escaping (_ result: Dictionary<String, Any>?, _ error: Error?) -> ()) {
        let headers = ["Authorization": Define.TOKEN_FIX_AUTHORIZATION,
                   "X-WALKING-API-KEY": Define.TOKEN_X_WALKING_API_KEY]
        Alamofire.request(stringURL,
                          method: .get,
                          parameters: nil,
                          encoding: URLEncoding.default,
                          headers: headers).responseJSON
        { response in
            switch response.result {
            case .success:
                let dictResult: Dictionary = response.value as! [String : AnyObject]
                callBack(dictResult,nil)
            case .failure(let encodingError):
                callBack(nil, encodingError)
            }
        }
    }
    
    // MARK: - POST Function.
    func postMethod(stringURL: String, parameters: Parameters, header: String?, callBack: @escaping (_ result: Dictionary<String, Any>?, _ error: Error?) -> ()){
        var headers: HTTPHeaders?
        if header == nil {
            headers = nil
        }else{
            headers = ["Authorization": Define.TOKEN_FIX_AUTHORIZATION,
                       "X-WALKING-API-KEY": Define.TOKEN_X_WALKING_API_KEY]
        }
        Alamofire.request(stringURL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON{
            response in
            switch response.result {
            case .success:
                let dictresult: Dictionary = response.value as! [String : Any]
                callBack(dictresult, nil)
            case .failure(let encodingError):
                callBack(nil, encodingError)
            }
        }
    }
    
    // MARK: - POST With Image Fuction.
    func postImageUplod(stringURL: String, parameters: Parameters, header: String?, imageName: String?, imageData: Data?, callBack: @escaping (_ result: Dictionary<String, Any>?, _ error: Error?) -> ()){
        var headers: HTTPHeaders?
        if header == nil {
            headers = nil
        }else{
            headers = ["Authorization": Define.TOKEN_FIX_AUTHORIZATION,
                       "X-WALKING-API-KEY": Define.TOKEN_X_WALKING_API_KEY]
        }
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            multipartFormData.append(imageData!, withName: imageName!, fileName: "Tiger.jpg", mimeType: "image/jpeg")
            
        }, usingThreshold: UInt64.init(), to: stringURL, method: .post, headers: headers, encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    let dictresult: Dictionary = response.value as! [String : Any]
                    callBack(dictresult, nil)
                }
            case .failure(let encodingError):
                print(encodingError)
                callBack(nil, encodingError)
            }
        })
    }
    
    // MARK: - POST Logout Function.
    func postLogoutMethod(stringURL: String, userID: String, header: String?, callBack: @escaping (_ result: Dictionary<String, Any>?, _ error: Error?) -> ()){
        var headers: HTTPHeaders?
        if header == nil {
            headers = nil
        }else{
            headers = ["Authorization": Define.TOKEN_FIX_AUTHORIZATION,
                       "X-WALKING-API-KEY": Define.TOKEN_X_WALKING_API_KEY,
                       "X-WALKING-LOGIN-TOKEN": header!,
                       "USER-ID": userID]
        }
        Alamofire.request(stringURL, method: .post, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON{
            response in
            switch response.result {
            case .success:
                let dictresult: Dictionary = response.value as! [String : Any]
                callBack(dictresult, nil)
            case .failure(let encodingError):
                callBack(nil, encodingError)
            }
        }
    }
    
    // MARK: - POST Change Password Function.
    func postChangePasswordMethod(stringURL: String, userID: String, parameter: Parameters, header: String?, callBack: @escaping (_ result: Dictionary<String, Any>?, _ error: Error?) -> ()){
        var headers: HTTPHeaders?
        if header == nil {
            headers = nil
        }else{
            headers = ["Authorization": Define.TOKEN_FIX_AUTHORIZATION,
                       "X-WALKING-API-KEY": Define.TOKEN_X_WALKING_API_KEY,
                       "X-WALKING-LOGIN-TOKEN": header!,
                       "USER-ID": userID]
        }
        Alamofire.request(stringURL, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: headers).responseJSON{
            response in
            switch response.result {
            case .success:
                let dictresult: Dictionary = response.value as! [String : Any]
                callBack(dictresult, nil)
            case .failure(let encodingError):
                callBack(nil, encodingError)
            }
        }
    }
    
    // MARK: - POST With Image Fuction For Offers.
    func postImageUplodOfferAndProfile(stringURL: String, parameters: Parameters, userID: String, header: String?, imageName: String?, imageData: Data?, callBack: @escaping (_ result: Dictionary<String, Any>?, _ error: Error?) -> ()){
        var headers: HTTPHeaders?
        if header == nil {
            headers = nil
        }else{
            headers = ["Authorization": Define.TOKEN_FIX_AUTHORIZATION,
                       "X-WALKING-API-KEY": Define.TOKEN_X_WALKING_API_KEY,
                       "X-WALKING-LOGIN-TOKEN": header!,
                       "USER-ID": userID]
        }
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if let dataImage = imageData {
                multipartFormData.append(dataImage, withName: imageName!, fileName: "Tiger.jpg", mimeType: "image/jpeg")
            }
            
        }, usingThreshold: UInt64.init(), to: stringURL, method: .post, headers: headers, encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    let dictresult: Dictionary = response.value as! [String : Any]
                    callBack(dictresult, nil)
                }
            case .failure(let encodingError):
                print(encodingError)
                callBack(nil, encodingError)
            }
        })
    }
    
    func postOptionalImageUplodOfferAndProfile(stringURL: String, parameters: Parameters, userID: String, header: String?, imageName: String?, imageData: Data?, callBack: @escaping (_ result: Dictionary<String, Any>?, _ error: Error?) -> ()){
        var headers: HTTPHeaders?
        if header == nil {
            headers = nil
        }else{
            headers = ["Authorization": Define.TOKEN_FIX_AUTHORIZATION,
                       "X-WALKING-API-KEY": Define.TOKEN_X_WALKING_API_KEY,
                       "X-WALKING-LOGIN-TOKEN": header!,
                       "USER-ID": userID]
        }
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            multipartFormData.append(imageData!, withName: imageName!, fileName: "Tiger.jpg", mimeType: "image/jpeg")
            
        }, usingThreshold: UInt64.init(), to: stringURL, method: .post, headers: headers, encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    let dictresult: Dictionary = response.value as! [String : Any]
                    callBack(dictresult, nil)
                }
            case .failure(let encodingError):
                print(encodingError)
                callBack(nil, encodingError)
            }
        })
    }
    
    // MARK: - POST Get Active Function.
    func postGetActiveOfferMethod(stringURL: String, userID: String, parameter: Parameters, header: String?, callBack: @escaping (_ result: Dictionary<String, Any>?, _ error: Error?) -> ()){
        var headers: HTTPHeaders?
        if header == nil {
            headers = nil
        }else{
            headers = ["Authorization": Define.TOKEN_FIX_AUTHORIZATION,
                       "X-WALKING-API-KEY": Define.TOKEN_X_WALKING_API_KEY,
                       "X-WALKING-LOGIN-TOKEN": header!,
                       "USER-ID": userID]
            print(headers!)
        }
        Alamofire.request(stringURL, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: headers).responseJSON{
            response in
            switch response.result {
            case .success:
                print(response)
                let dictresult: Dictionary = response.value as! [String : Any]
                callBack(dictresult, nil)
            case .failure(let encodingError):
                callBack(nil, encodingError)
            }
        }
    }
    
    // MARK: - POST Get Active Function.
    func postGetActiveUserMethod(stringURL: String, userID: String, parameter: Parameters, header: String?, callBack: @escaping (_ result: Dictionary<String, Any>?, _ error: Error?) -> ()){
        var headers: HTTPHeaders?
        if header == nil {
            headers = nil
        }else{
            headers = ["Authorization": Define.TOKEN_FIX_AUTHORIZATION,
                       "X-WALKING-API-KEY": Define.TOKEN_X_WALKING_API_KEY,
                       "X-WALKING-LOGIN-TOKEN": header!,
                       "USER-ID": userID]
        }
        Alamofire.request(stringURL, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: headers).responseJSON{
            response in
            switch response.result {
            case .success:
                let dictresult: Dictionary = response.value as! [String : Any]
                callBack(dictresult, nil)
            case .failure(let encodingError):
                callBack(nil, encodingError)
            }
        }
    }
    
    // MARK: - POST Get Active Function.
    func postAddUpdateWalkerOfferMethod(stringURL: String, userID: String, parameter: Parameters, header: String?, callBack: @escaping (_ result: Dictionary<String, Any>?, _ error: Error?) -> ()){
        var headers: HTTPHeaders?
        if header == nil {
            headers = nil
        }else{
            headers = ["Authorization": Define.TOKEN_FIX_AUTHORIZATION,
                       "X-WALKING-API-KEY": Define.TOKEN_X_WALKING_API_KEY,
                       "X-WALKING-LOGIN-TOKEN": header!,
                       "USER-ID": userID]
        }
        Alamofire.request(stringURL, method: .post, parameters: parameter, encoding: URLEncoding.default, headers: headers).responseJSON{
            response in
            switch response.result {
            case .success:
                let dictresult: Dictionary = response.value as! [String : Any]
                callBack(dictresult, nil)
            case .failure(let encodingError):
                callBack(nil, encodingError)
            }
        }
    }
}
